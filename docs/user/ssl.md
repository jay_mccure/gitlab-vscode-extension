# GitLab instance with self-signed certificate

This page has moved [into the GitLab documentation](https://docs.gitlab.com/ee/editor_extensions/visual_studio_code/ssl.html).

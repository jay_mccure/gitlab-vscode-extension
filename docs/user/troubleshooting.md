# Troubleshooting the GitLab extension

This page has moved [into the GitLab documentation](https://docs.gitlab.com/ee/editor_extensions/visual_studio_code/troubleshooting.html).

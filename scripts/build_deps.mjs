import path from 'node:path';
import { rimraf } from 'rimraf';
import { root, run } from './utils/run_utils.mjs';

/**
 * Transforms dependencies using ES6 modules
 * to CommonJS. Otherwise, we can't require
 * these dependencies from CommonJS modules
 * without using dynamic imports.
 */
async function main() {
  await rimraf([
    path.join(root, 'dist-desktop/node_modules/@anycable'),
    path.join(root, 'dist-desktop/node_modules/nanoevents'),
  ]);
  await run('babel', [
    path.join(root, 'node_modules/@anycable'),
    '--out-dir',
    path.join(root, 'dist-desktop/node_modules/@anycable'),
  ]);
  await run('babel', [
    path.join(root, 'node_modules/nanoevents'),
    '--out-dir',
    path.join(root, 'dist-desktop/node_modules/nanoevents'),
  ]);
}

main();

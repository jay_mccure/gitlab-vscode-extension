import { readFileSync } from 'node:fs';
import { join } from 'node:path';
import lodash from 'lodash';
import codeSuggestionsConfig from './code_suggestions_settings.cjs';
import { root } from './run_utils.mjs';

const packageJson = () => JSON.parse(readFileSync(join(root, 'package.json')));
const desktopPackageJson = () => JSON.parse(readFileSync(join(root, 'desktop.package.json')));
const browserPackageJson = () => JSON.parse(readFileSync(join(root, 'browser.package.json')));

function mergeJson(left, right) {
  return lodash.mergeWith(left, right, (src, other) =>
    lodash.isArray(src) && lodash.isArray(other) ? src.concat(other) : undefined,
  );
}

const buildSupportedLanguages = () => {
  const enabledSupportedLanguages = {};
  const defaultValues = {};
  const { supportedLanguages } = codeSuggestionsConfig.default;
  for (const language of supportedLanguages) {
    enabledSupportedLanguages[language.languageId] = {
      type: 'boolean',
      default: true,
      description: language.humanReadableName,
    };

    defaultValues[language.languageId] = true;
  }

  return {
    contributes: {
      configuration: {
        properties: {
          'gitlab.duoCodeSuggestions.enabledSupportedLanguages': {
            description: 'Enable Code Suggestions for these languages.',
            type: 'object',
            order: 1,
            properties: enabledSupportedLanguages,
            default: defaultValues,
            additionalProperties: false,
          },
        },
      },
    },
  };
};

export function prettyPrint(json) {
  return JSON.stringify(json, null, 2);
}

export function createDesktopPackageJson() {
  return mergeJson(mergeJson(packageJson(), buildSupportedLanguages()), desktopPackageJson());
}

export function createBrowserPackageJson() {
  return mergeJson(mergeJson(packageJson(), buildSupportedLanguages()), browserPackageJson());
}

const { Uri } = require('../desktop/test_utils/uri');
const { EventEmitter } = require('../desktop/test_utils/event_emitter');
const { FileType } = require('../desktop/test_utils/file_type');
const { FileSystemError } = require('../desktop/test_utils/file_system_error');
const { Position } = require('../common/test_utils/position');
const { TabInputText } = require('../common/test_utils/tab_input_text');

const returnsDisposable = () => jest.fn().mockReturnValue({ dispose: jest.fn() });

module.exports = {
  Disposable: {
    from: function from(...items) {
      return {
        dispose: () => {
          items.forEach(i => i.dispose());
        },
      };
    },
  },
  TreeItem: function TreeItem(labelOrUri, collapsibleState) {
    this.collapsibleState = collapsibleState;
    if (typeof labelOrUri === 'string') {
      this.label = labelOrUri;
    } else {
      this.resourceUri = labelOrUri;
    }
  },
  ThemeIcon: function ThemeIcon(id) {
    return { id };
  },
  EventEmitter,
  TreeItemCollapsibleState: {
    Collapsed: 'collapsed',
  },
  MarkdownString: function MarkdownString(value, supportThemeIcons) {
    this.value = value;
    this.supportThemeIcons = supportThemeIcons;
  },
  Uri,
  TabInputText,
  authentication: {
    getSession: jest.fn(),
    onDidChangeSessions: jest.fn(),
  },
  comments: {
    createCommentController: jest.fn(),
  },
  window: {
    showInformationMessage: jest.fn(),
    showWarningMessage: jest.fn(),
    showErrorMessage: jest.fn(),
    createStatusBarItem: jest.fn(),
    showInputBox: jest.fn(),
    showQuickPick: jest.fn(),
    showSaveDialog: jest.fn(),
    withProgress: jest.fn().mockImplementation((opt, callback) => callback()),
    createQuickPick: jest.fn(),
    onDidChangeTextEditorSelection: jest.fn(),
    onDidChangeVisibleTextEditors: jest.fn(),
    onDidChangeTextEditorVisibleRanges: jest.fn(),
    onDidChangeActiveTextEditor: jest.fn(),
    onDidChangeActiveColorTheme: jest.fn(),
    createWebviewPanel: jest.fn(),
    showTextDocument: jest.fn(),
    tabGroups: {
      activeTabGroup: {},
      all: [],
    },
    createTextEditorDecorationType: jest.fn(),
    registerWebviewViewProvider: jest.fn(),
  },
  commands: {
    executeCommand: jest.fn(),
    registerCommand: returnsDisposable(),
  },
  languages: {
    registerInlineCompletionItemProvider: returnsDisposable(),
    registerCompletionItemProvider: jest.fn(),
  },
  workspace: {
    openTextDocument: jest.fn(),
    getConfiguration: jest.fn().mockReturnValue({ get: jest.fn() }),
    onDidOpenTextDocument: jest.fn(),
    onDidChangeConfiguration: returnsDisposable(),
    onDidChangeTextDocument: jest.fn(),
    onDidCloseTextDocument: jest.fn(),
    createFileSystemWatcher: jest.fn(),
    fs: {
      readFile: jest.fn(),
      writeFile: jest.fn(),
    },
  },
  extensions: {
    getExtension: jest.fn(),
  },
  env: {
    isTelemetryEnabled: true,
    uriScheme: 'vscode',
    clipboard: {
      writeText: jest.fn(),
    },
    onDidChangeTelemetryEnabled: jest.fn(),
    openExternal: jest.fn(),
  },
  CommentMode: { Editing: 0, Preview: 1 },
  StatusBarAlignment: { Left: 0 },
  CommentThreadCollapsibleState: { Collapsed: 0, Expanded: 1 },
  CommentThreadState: { Unresolved: 0, Resolved: 1 },
  Position,
  Selection: jest.fn(),
  Range: function Range(...args) {
    if (typeof args[0] === 'number') {
      return {
        start: { line: args[0], character: args[1] },
        end: { line: args[2], character: args[3] },
      };
    }
    return { start: args[0], end: args[1] };
  },
  CancellationTokenSource: function CancellationTokenSource() {
    const controller = new AbortController();

    return {
      token: {
        get isCancellationRequested() {
          return controller.signal.aborted;
        },
        set isCancellationRequested(val) {
          throw new Error(
            'Cannot set isCancellationRequested. Try using the CancellationTokenSource.',
          );
        },
        onCancellationRequested(callback) {
          controller.signal.addEventListener('abort', callback);

          return {
            dispose() {
              controller.signal.removeEventListener('abort', callback);
            },
          };
        },
      },
      cancel() {
        controller.abort();
      },
    };
  },
  ThemeColor: jest.fn(color => color),
  ProgressLocation: {
    Notification: 'Notification',
  },
  FoldingRange: function FoldingRange(start, end, kind) {
    return { start, end, kind };
  },
  FoldingRangeKind: {
    Region: 3,
  },
  FileType,
  FileSystemError,
  ViewColumn: {
    Active: -1,
  },
  InlineCompletionTriggerKind: {
    Automatic: true,
  },
  CompletionItemKind: {
    Snippet: 14,
    Text: 0,
  },
  InlineCompletionItem: function InlineCompletionItem(insertText, range, command) {
    this.insertText = insertText;
    this.range = range;
    this.command = command;
  },
  ConfigurationTarget: {
    Global: 1,
    Workspace: 2,
    WorkspaceFolder: 3,
  },
  CompletionItem: function CompletionItem(label, kind) {
    return { label, kind };
  },
  CodeLens: jest.fn(),
  DocumentLink: jest.fn(),
  CodeAction: jest.fn(),
  Diagnostic: jest.fn(),
  CallHierarchyItem: jest.fn(),
  TypeHierarchyItem: jest.fn(),
  SymbolInformation: jest.fn(),
  InlayHint: jest.fn(),
  CancellationError: jest.fn(),
  QuickPickItemKind: { Separator: -1 },
  SnippetString: jest.fn(),
  version: 'vscode-test-version-0.0',
};

import * as vscode from 'vscode';
import { GitLabChatController } from '../gitlab_chat_controller';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import { fixCode } from './fix_code';

let selectedTextValue: string | null = 'selectedText';
const filenameValue = 'filename';

jest.mock('../utils/editor_text_utils', () => ({
  getSelectedText: jest.fn().mockImplementation(() => selectedTextValue),
  getActiveFileName: jest.fn().mockImplementation(() => filenameValue),
  getTextAfterSelected: jest.fn().mockReturnValue('textAfterSelection'),
  getTextBeforeSelected: jest.fn().mockReturnValue('textBeforeSelection'),
}));

describe('fix', () => {
  let controller: GitLabChatController;

  beforeEach(() => {
    controller = createFakePartial<GitLabChatController>({
      processNewUserRecord: jest.fn(),
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('creates new record with selection', async () => {
    selectedTextValue = 'hello';

    await fixCode(controller);
    expect(controller.processNewUserRecord).toHaveBeenCalledWith(
      expect.objectContaining({
        content: '/fix',
        role: 'user',
        type: 'fixCode',
        context: {
          currentFile: {
            selectedText: selectedTextValue,
            fileName: filenameValue,
            contentAboveCursor: 'textBeforeSelection',
            contentBelowCursor: 'textAfterSelection',
          },
        },
      }),
    );
  });

  it('does not create new record if there is no active editor', async () => {
    vscode.window.activeTextEditor = undefined;

    await fixCode(controller);

    expect(controller.processNewUserRecord).not.toHaveBeenCalled();
  });

  it('does not create new record if there is no selected text', async () => {
    selectedTextValue = '';

    await fixCode(controller);

    expect(controller.processNewUserRecord).not.toHaveBeenCalled();
  });
});

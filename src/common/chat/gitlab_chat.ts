import * as vscode from 'vscode';
import { GitLabPlatformManager } from '../platform/gitlab_platform';
import { QuickChat } from '../quick_chat/quick_chat';
import {
  COMMAND_OPEN_QUICK_CHAT,
  COMMAND_SEND_QUICK_CHAT,
  COMMAND_SEND_QUICK_CHAT_DUPLICATE,
  COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT,
} from '../quick_chat/constants';
import { GitLabChatController } from './gitlab_chat_controller';
import { COMMAND_OPEN_GITLAB_CHAT, openGitLabChat } from './commands/open_gitlab_chat';
import {
  COMMAND_EXPLAIN_SELECTED_CODE,
  explainSelectedCode,
} from './commands/explain_selected_code';
import { COMMAND_GENERATE_TESTS, generateTests } from './commands/generate_tests';
import {
  COMMAND_NEW_CHAT_CONVERSATION,
  newChatConversation,
} from './commands/new_chat_conversation';
import { COMMAND_FIX_CODE, fixCode } from './commands/fix_code';
import { GitLabPlatformManagerForChat } from './get_platform_manager_for_chat';
import { CHAT_SIDEBAR_VIEW_ID } from './gitlab_chat_view';
import { COMMAND_REFACTOR_CODE, refactorCode } from './commands/refactor_code';
import { isDuoChatAvailable } from './utils/chat_availability_utils';
import { AIContextManager } from './ai_context_manager';

const setChatAvailable = async (manager: GitLabPlatformManager) => {
  await vscode.commands.executeCommand(
    'setContext',
    'gitlab:chatAvailable',
    await isDuoChatAvailable(manager),
  );
};

export const activateChat = async (
  context: vscode.ExtensionContext,
  manager: GitLabPlatformManager,
  aiContextManager: AIContextManager,
) => {
  const platformManagerForChat = new GitLabPlatformManagerForChat(manager);
  const controller = new GitLabChatController(platformManagerForChat, context, aiContextManager);
  const quickChat = new QuickChat(platformManagerForChat, context, aiContextManager);

  await setChatAvailable(manager);

  manager.onAccountChange(async () => {
    await setChatAvailable(manager);
  });

  context.subscriptions.push(
    // sidebar view
    vscode.window.registerWebviewViewProvider(CHAT_SIDEBAR_VIEW_ID, controller),
    // inline chat
    quickChat,
  );

  // commands
  context.subscriptions.push(
    vscode.commands.registerCommand(COMMAND_OPEN_GITLAB_CHAT, async () => {
      await openGitLabChat(controller);
    }),
    vscode.commands.registerCommand(COMMAND_EXPLAIN_SELECTED_CODE, async () => {
      await explainSelectedCode(controller);
    }),
    vscode.commands.registerCommand(COMMAND_GENERATE_TESTS, async () => {
      await generateTests(controller);
    }),
    vscode.commands.registerCommand(COMMAND_REFACTOR_CODE, async () => {
      await refactorCode(controller);
    }),
    vscode.commands.registerCommand(COMMAND_FIX_CODE, async () => {
      await fixCode(controller);
    }),
    vscode.commands.registerCommand(COMMAND_NEW_CHAT_CONVERSATION, async () => {
      await newChatConversation(controller);
    }),
    vscode.commands.registerCommand(COMMAND_OPEN_QUICK_CHAT, () => quickChat.show()),
    vscode.commands.registerCommand(COMMAND_SEND_QUICK_CHAT, (reply: vscode.CommentReply) =>
      quickChat.send(reply),
    ),
    vscode.commands.registerCommand(
      COMMAND_SEND_QUICK_CHAT_DUPLICATE,
      (reply: vscode.CommentReply) => quickChat.send(reply),
    ),
    vscode.commands.registerCommand(COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT, async ({ code }) => {
      await vscode.env.clipboard.writeText(code);
      await vscode.window.showInformationMessage('Code copied to clipboard!');
    }),
  );
};

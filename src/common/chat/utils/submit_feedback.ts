import { Snowplow } from '../../snowplow/snowplow';
import { GitLabEnvironment } from '../get_platform_manager_for_chat';

const GITLAB_STANDARD_SCHEMA_URL = 'iglu:com.gitlab/gitlab_standard/jsonschema/1-0-9';

function buildStandardContext(
  improveWhat: string | null,
  didWhat: string | null,
  gitlabEnvironment: GitLabEnvironment,
) {
  return {
    schema: GITLAB_STANDARD_SCHEMA_URL,
    data: {
      source: 'gitlab-vscode',
      extra: {
        improveWhat,
        didWhat,
      },
      environment: gitlabEnvironment,
    },
  };
}

export type SubmitFeedbackParams = {
  improveWhat: string | null;
  didWhat: string | null;
  feedbackChoices: string[] | null;
  gitlabEnvironment: GitLabEnvironment;
};

export const submitFeedback = async ({
  didWhat,
  improveWhat,
  feedbackChoices,
  gitlabEnvironment,
}: SubmitFeedbackParams) => {
  const hasFeedback = Boolean(didWhat) || Boolean(improveWhat) || Boolean(feedbackChoices?.length);

  if (!hasFeedback) {
    return;
  }

  const standardContext = buildStandardContext(improveWhat, didWhat, gitlabEnvironment);

  await Snowplow.getInstance().trackStructEvent(
    {
      category: 'ask_gitlab_chat',
      action: 'click_button',
      label: 'response_feedback',
      property: feedbackChoices?.join(','),
    },
    [standardContext, 'ide-extension-context'],
  );
};

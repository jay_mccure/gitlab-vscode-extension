import * as vscode from 'vscode';
import { StatusBarItemUI, createStatusBarItem } from '../utils/status_bar_item';
import { SHOW_QUICK_PICK_MENU } from '../duo_quick_pick/commands/show_quick_pick_menu';
import {
  CodeSuggestionsStateManager,
  VisibleCodeSuggestionsState,
} from './code_suggestions_state_manager';

export const CODE_SUGGESTION_LABEL = 'Duo';
export const CODE_SUGGESTIONS_STATUSES: Record<VisibleCodeSuggestionsState, StatusBarItemUI> = {
  [VisibleCodeSuggestionsState.DISABLED_VIA_SETTINGS]: {
    iconName: 'gitlab-code-suggestions-disabled',
    tooltip: 'Code suggestions are disabled',
  },
  [VisibleCodeSuggestionsState.DISABLED_LANGUAGE_VIA_SETTINGS]: {
    iconName: 'gitlab-code-suggestions-disabled',
    tooltip: 'Code suggestions are disabled for this language',
  },
  [VisibleCodeSuggestionsState.READY]: {
    iconName: 'gitlab-code-suggestions-enabled',
    tooltip: 'Code suggestions are enabled',
  },
  [VisibleCodeSuggestionsState.UNSUPPORTED_LANGUAGE]: {
    iconName: 'gitlab-code-suggestions-disabled',
    tooltip: 'Code suggestions are not supported for this language',
  },
  [VisibleCodeSuggestionsState.DISABLED_BY_USER]: {
    iconName: 'gitlab-code-suggestions-disabled',
    tooltip: 'Code suggestions are disabled per user request. Click to enable',
  },
  [VisibleCodeSuggestionsState.NO_ACCOUNT]: {
    iconName: 'gitlab-code-suggestions-disabled',
    tooltip: 'Code suggestions are disabled because there is no user account',
  },
  [VisibleCodeSuggestionsState.NO_LICENSE]: {
    iconName: 'gitlab-code-suggestions-disabled',
    tooltip:
      'Code Suggestions is now a paid feature, part of Duo Pro. Contact your GitLab administrator to upgrade',
  },
  [VisibleCodeSuggestionsState.DISABLED_BY_PROJECT]: {
    iconName: 'gitlab-code-suggestions-disabled',
    tooltip: 'Code suggestions are disabled for this project',
  },
  [VisibleCodeSuggestionsState.UNSUPPORTED_GITLAB_VERSION]: {
    iconName: 'gitlab-code-suggestions-disabled',
    tooltip: 'GitLab Duo Code Suggestions requires GitLab version 16.8 or later.',
  },
};

export class CodeSuggestionsStatusBarItem {
  codeSuggestionsStatusBarItem?: vscode.StatusBarItem;

  #codeSuggestionsStateSubscription?: vscode.Disposable;

  updateCodeSuggestionsItem(state: VisibleCodeSuggestionsState) {
    if (!this.codeSuggestionsStatusBarItem) return;
    if (
      state === VisibleCodeSuggestionsState.LOADING ||
      state === VisibleCodeSuggestionsState.ERROR
    )
      return;

    const newUiState = CODE_SUGGESTIONS_STATUSES[state];

    this.codeSuggestionsStatusBarItem.text = `$(${newUiState.iconName}) ${CODE_SUGGESTION_LABEL}`;
    this.codeSuggestionsStatusBarItem.tooltip = newUiState.tooltip;
  }

  constructor(state: CodeSuggestionsStateManager) {
    this.codeSuggestionsStatusBarItem = createCodeSuggestionStatusBarItem();
    this.updateCodeSuggestionsItem(state.getVisibleState());
    this.#codeSuggestionsStateSubscription = state.onDidChangeVisibleState(e =>
      this.updateCodeSuggestionsItem(e),
    );
  }

  dispose(): void {
    this.#codeSuggestionsStateSubscription?.dispose();
  }
}

function createCodeSuggestionStatusBarItem() {
  return createStatusBarItem({
    priority: Number.MAX_VALUE,
    id: 'gl.status.code_suggestions',
    name: 'GitLab Workflow: Code Suggestions',
    initialText: '$(gitlab-code-suggestions-disabled)',
    command: SHOW_QUICK_PICK_MENU,
    alignment: vscode.StatusBarAlignment.Right,
  });
}

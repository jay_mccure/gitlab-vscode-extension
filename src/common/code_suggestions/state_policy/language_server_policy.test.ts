import vscode from 'vscode';
import { BaseLanguageClient } from 'vscode-languageclient';
import {
  CHAT,
  CODE_SUGGESTIONS,
  DUO_DISABLED_FOR_PROJECT,
  UNSUPPORTED_GITLAB_VERSION,
  FeatureStateNotificationParams,
  StateCheckId,
} from '@gitlab-org/gitlab-lsp';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import { createExtensionContext } from '../../test_utils/entities';
import { DO_NOT_SHOW_CODE_SUGGESTIONS_VERSION_WARNING } from '../../constants';
import { LanguageServerPolicy, UNSUPPORTED_LANGUAGE } from './language_server_policy';

const waitForPromises = () => new Promise(process.nextTick);

describe('LanguageServerPolicy', () => {
  let policy: LanguageServerPolicy;
  let context: vscode.ExtensionContext;
  const mockOnNotification = jest.fn();
  const mockOnNotificationDisposable = jest.fn();
  const client = createFakePartial<BaseLanguageClient>({
    onNotification: mockOnNotification,
    sendNotification: jest.fn(),
  });
  let sendNotification: (params: FeatureStateNotificationParams) => void;
  const engageListener = jest.fn();

  const engagedPolicyParams = createFakePartial<FeatureStateNotificationParams>([
    {
      featureId: CODE_SUGGESTIONS,
      engagedChecks: [
        {
          checkId: UNSUPPORTED_LANGUAGE,
          details: 'test',
        },
      ],
    },
  ]);

  const anotherEngagedPolicyParams = createFakePartial<FeatureStateNotificationParams>([
    {
      featureId: CODE_SUGGESTIONS,
      engagedChecks: [
        {
          checkId: 'test' as StateCheckId,
          details: 'test',
        },
      ],
    },
  ]);

  const notEngagedParams = createFakePartial<FeatureStateNotificationParams>([]);

  beforeEach(async () => {
    context = createExtensionContext();
    jest.resetAllMocks();
    policy = new LanguageServerPolicy(client, context);
    mockOnNotification.mockImplementation((_, _callback) => {
      sendNotification = _callback;
      return { dispose: mockOnNotificationDisposable };
    });
    await policy.init();

    policy.onEngagedChange(engageListener);
  });

  it('is engaged when LS notifies about the code suggestions state change with NON-empty state', () => {
    sendNotification(engagedPolicyParams);

    expect(policy.engaged).toBe(true);
    expect(policy.state).toBe(UNSUPPORTED_LANGUAGE);
  });

  it('is NOT engaged when LS notifies about the code suggestions state with an empty state', () => {
    const params = createFakePartial<FeatureStateNotificationParams>([]);
    sendNotification(params);

    expect(policy.engaged).toBe(false);
    expect(policy.state).toBe(undefined);
  });

  it('fires onEngagedChange after updating the policy', () => {
    engageListener.mockImplementation(() => {
      expect(policy.engaged).toBe(true);
      expect(policy.state).toBe(UNSUPPORTED_LANGUAGE);
    });

    expect(engageListener).not.toHaveBeenCalled();

    sendNotification(engagedPolicyParams);

    expect(engageListener).toHaveBeenCalledTimes(1);
  });

  it('fires onEngagedChange only when state changes', () => {
    sendNotification(notEngagedParams);
    sendNotification(notEngagedParams);
    sendNotification(engagedPolicyParams); // triggers change -> true
    sendNotification(engagedPolicyParams);
    sendNotification(anotherEngagedPolicyParams); // triggers change -> true
    sendNotification(notEngagedParams); // triggers change -> false

    expect(engageListener.mock.calls).toEqual([[true], [true], [false]]);
  });

  it('disposes the client notification disposable', () => {
    expect(mockOnNotificationDisposable).not.toHaveBeenCalled();

    policy.dispose();

    expect(mockOnNotificationDisposable).toHaveBeenCalledTimes(1);
  });

  describe('Special state handling', () => {
    describe(`${UNSUPPORTED_GITLAB_VERSION}`, () => {
      const baseUrl = 'http://test.com';
      const version = '16.7';

      const engagedMinGitLabVersionCheckParams = createFakePartial<FeatureStateNotificationParams>([
        {
          featureId: CODE_SUGGESTIONS,
          engagedChecks: [
            {
              checkId: UNSUPPORTED_GITLAB_VERSION,
              context: {
                baseUrl,
                version,
              },
            },
          ],
        },
      ]);

      it(`shows error notification when version is below 16.8`, async () => {
        await sendNotification(engagedMinGitLabVersionCheckParams);
        expect(vscode.window.showWarningMessage).toHaveBeenCalled();
      });

      it('stores user preference for not showing the warning', async () => {
        (vscode.window.showWarningMessage as jest.Mock).mockResolvedValue('Do not show again');

        await sendNotification(engagedMinGitLabVersionCheckParams);

        expect(context.globalState.get(DO_NOT_SHOW_CODE_SUGGESTIONS_VERSION_WARNING)).toStrictEqual(
          {
            [baseUrl]: true,
          },
        );
      });

      it('does not show the warning if user said they do not want to see it but disabled code suggestions', async () => {
        await context.globalState.update(DO_NOT_SHOW_CODE_SUGGESTIONS_VERSION_WARNING, {
          [baseUrl]: true,
        });

        await sendNotification(engagedMinGitLabVersionCheckParams);

        expect(vscode.window.showWarningMessage).not.toHaveBeenCalled();
      });
    });
  });

  describe('Chat state', () => {
    it('should run command to disable chat when LS policy is engaged', async () => {
      const chatDisabledParams = createFakePartial<FeatureStateNotificationParams>([
        {
          featureId: CHAT,
          engagedChecks: [
            {
              checkId: DUO_DISABLED_FOR_PROJECT,
            },
          ],
        },
      ]);

      sendNotification(chatDisabledParams);
      await waitForPromises();
      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        'setContext',
        'gitlab:chatAvailableForProject',
        false,
      );
    });

    it('should run command to enable chat when no chat-related policy is engaged', async () => {
      const chatDisabledParams = createFakePartial<FeatureStateNotificationParams>([
        {
          featureId: CODE_SUGGESTIONS,
          engagedChecks: [
            {
              checkId: UNSUPPORTED_LANGUAGE,
            },
          ],
        },
        {
          featureId: CHAT,
          engagedChecks: [],
        },
      ]);

      sendNotification(chatDisabledParams);
      await waitForPromises();
      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        'setContext',
        'gitlab:chatAvailableForProject',
        true,
      );
    });
  });
});

export const VS_COMMANDS = {
  DIFF: 'vscode.diff',
  OPEN: 'vscode.open',
  GIT_SHOW_OUTPUT: 'git.showOutput',
  GIT_CLONE: 'git.clone',
  MARKDOWN_SHOW_PREVIEW: 'markdown.showPreview',
  OPEN_SETTINGS: 'workbench.action.openSettings',
};

export const USER_COMMANDS = {
  AUTHENTICATE: 'gl.authenticate',
};

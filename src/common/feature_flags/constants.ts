// Add your feature flag here
export enum FeatureFlag {
  // used for testing purposes
  TestFlag = 'testflag',
  RemoteSecurityScans = 'remoteSecurityScans',
  SecurityScans = 'securityScansFlag',
  ForceCodeSuggestionsViaMonolith = 'forceCodeSuggestionsViaMonolith',
  LanguageServer = 'languageServer',
  LanguageServerWebIDE = 'languageServerWebIDE',
  CodeSuggestionsClientDirectToGateway = 'codeSuggestionsClientDirectToGateway',
  StreamCodeGenerations = 'streamCodeGenerations',
  CodeSuggestionsLicensePolicy = 'codeSuggestionsLicensePolicy',
  LanguageServerWebviews = 'languageServerWebviews',
}

// Set the feature flag default value here
export const FEATURE_FLAGS_DEFAULT_VALUES = {
  [FeatureFlag.RemoteSecurityScans]: false,
  [FeatureFlag.SecurityScans]: true,
  [FeatureFlag.ForceCodeSuggestionsViaMonolith]: false,
  [FeatureFlag.TestFlag]: false,
  [FeatureFlag.LanguageServer]: true,
  [FeatureFlag.CodeSuggestionsClientDirectToGateway]: true,
  [FeatureFlag.LanguageServerWebIDE]: false,
  [FeatureFlag.StreamCodeGenerations]: true,
  [FeatureFlag.CodeSuggestionsLicensePolicy]: true,
  [FeatureFlag.LanguageServerWebviews]: false,
};

// PLEASE NOTE: We can only query 20 flags at a time so this list shouldn't grow past that.
// https://gitlab.com/gitlab-org/gitlab/-/blob/933b5643feebe1feb471be2652d98497c17bc65b/app/graphql/resolvers/app_config/gitlab_instance_feature_flags_resolver.rb#L7
export enum InstanceFeatureFlag {
  DuoWorkflow = 'duo_workflow',
}

export const INSTANCE_FEATURE_FLAGS = Object.values(InstanceFeatureFlag);

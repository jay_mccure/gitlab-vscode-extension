import { mapKeys, mapValues } from 'lodash';
import * as vscode from 'vscode';
import {
  ExtensionConfiguration,
  getExtensionConfiguration,
} from '../utils/extension_configuration';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { GitLabPlatformForAccount, GitLabPlatformManager } from '../platform/gitlab_platform';
import { gitlabPlatformForAccount } from '../test_utils/entities';
import {
  createFakeFetchFromApi,
  FakeRequestHandler,
} from '../test_utils/create_fake_fetch_from_api';
import {
  FeatureFlagService,
  isEnabled,
  FeatureFlag,
  FEATURE_FLAGS_DEFAULT_VALUES,
  getInstanceFeatureFlagsRequest,
} from './feature_flag_service';
import { INSTANCE_FEATURE_FLAGS } from './constants';

jest.mock('../utils/extension_configuration');

const VSCODE_CONTEXT_DEFAULT_LOCAL_FLAGS = mapKeys(
  FEATURE_FLAGS_DEFAULT_VALUES,
  (_, name) => `gitlab.featureFlags.${name}`,
);
const VSCODE_CONTEXT_DEFAULT_INSTANCE_FLAGS = Object.fromEntries(
  INSTANCE_FEATURE_FLAGS.map(name => [`gitlab.featureFlags.${name}`, false]),
);

describe('feature_flags/index', () => {
  let platform: GitLabPlatformForAccount;
  let platformManager: GitLabPlatformManager;
  let featureFlagService: FeatureFlagService;

  const mockExtensionConfiguration = (featureFlags = {}) => {
    jest.mocked(getExtensionConfiguration).mockReturnValue(
      createFakePartial<ExtensionConfiguration>({
        debug: false,
        featureFlags,
        ignoreCertificateErrors: false,
        customQueries: [],
      }),
    );
  };

  const setupFetchHandlers = (...requestHandlers: FakeRequestHandler<unknown>[]) => {
    platform = {
      ...platform,
      fetchFromApi: createFakeFetchFromApi(...requestHandlers),
    };
  };

  const getVSCodeContext = () => {
    const actualCalls = jest.mocked(vscode.commands.executeCommand).mock.calls;
    const entries = actualCalls
      .filter(([command]) => command === 'setContext')
      .map(([, name, value]) => [name, value]);

    return Object.fromEntries(entries);
  };

  const triggerDidChangeConfiguration = (e: vscode.ConfigurationChangeEvent) => {
    const promises = jest
      .mocked(vscode.workspace.onDidChangeConfiguration)
      .mock.calls.map(([listener]) => listener(e));

    return Promise.all(promises);
  };

  const triggerOnAccountChange = () => {
    const promises = jest
      .mocked(platformManager.onAccountChange)
      .mock.calls.map(([listener]) => listener());

    return Promise.all(promises);
  };

  beforeEach(() => {
    mockExtensionConfiguration();

    platform = gitlabPlatformForAccount;

    platformManager = createFakePartial<GitLabPlatformManager>({
      onAccountChange: jest.fn(),
      getForActiveAccount: jest.fn().mockImplementation(() => Promise.resolve(platform)),
    });

    featureFlagService = new FeatureFlagService(platformManager);
  });

  // PLEASE NOTE: We can only query 20 flags at a time so this list shouldn't grow past that.
  // https://gitlab.com/gitlab-org/gitlab/-/blob/933b5643feebe1feb471be2652d98497c17bc65b/app/graphql/resolvers/app_config/gitlab_instance_feature_flags_resolver.rb#L7
  it('should not have more than 20 instance flags', () => {
    expect(INSTANCE_FEATURE_FLAGS.length).toBeLessThanOrEqual(20);
  });

  describe('isEnabled', () => {
    it.each`
      configurationValue | defaultValue | enabled
      ${undefined}       | ${true}      | ${true}
      ${undefined}       | ${false}     | ${false}
      ${true}            | ${true}      | ${true}
      ${true}            | ${false}     | ${true}
      ${false}           | ${true}      | ${false}
      ${false}           | ${false}     | ${false}
    `(
      'returns $outcome when config = $configurationValue && default = $defaultValue',
      ({ configurationValue, defaultValue, enabled }) => {
        mockExtensionConfiguration({ [FeatureFlag.TestFlag]: configurationValue });
        FEATURE_FLAGS_DEFAULT_VALUES[FeatureFlag.TestFlag] = defaultValue;

        expect(isEnabled(FeatureFlag.TestFlag)).toBe(enabled);
      },
    );
  });

  describe('FeatureFlagService', () => {
    describe('with instance flags set', () => {
      beforeEach(() => {
        mockExtensionConfiguration({ [FeatureFlag.TestFlag]: true });
        setupFetchHandlers({
          request: getInstanceFeatureFlagsRequest(INSTANCE_FEATURE_FLAGS),
          response: {
            metadata: {
              featureFlags: INSTANCE_FEATURE_FLAGS.map(name => ({ name, enabled: true })),
            },
          },
        });
      });

      it('sets the local and instance-level feature flag state', async () => {
        expect(getVSCodeContext()).toEqual({});

        await featureFlagService.init();

        expect(getVSCodeContext()).toEqual({
          ...VSCODE_CONTEXT_DEFAULT_LOCAL_FLAGS,
          ...mapValues(VSCODE_CONTEXT_DEFAULT_INSTANCE_FLAGS, () => true),
          'gitlab.featureFlags.testflag': true,
        });
      });
    });

    describe('with instance flags response empty', () => {
      beforeEach(() => {
        setupFetchHandlers({
          request: getInstanceFeatureFlagsRequest(INSTANCE_FEATURE_FLAGS),
          response: {
            metadata: {
              featureFlags: [],
            },
          },
        });
      });

      it('sets the local and instance-level feature flag state', async () => {
        await featureFlagService.init();

        expect(getVSCodeContext()).toEqual({
          ...VSCODE_CONTEXT_DEFAULT_LOCAL_FLAGS,
          ...VSCODE_CONTEXT_DEFAULT_INSTANCE_FLAGS,
        });
      });
    });

    describe('when a feature flag setting changes', () => {
      it('when affectsConfiguration is false, it does nothing', async () => {
        await featureFlagService.init();
        jest.mocked(vscode.commands.executeCommand).mockClear();

        await triggerDidChangeConfiguration({ affectsConfiguration: () => false });

        expect(getVSCodeContext()).toEqual({});
      });

      it('when affectsConfiguration is true, it resets the local feature flags', async () => {
        await featureFlagService.init();
        jest.mocked(vscode.commands.executeCommand).mockClear();

        await triggerDidChangeConfiguration({ affectsConfiguration: () => true });

        expect(getVSCodeContext()).toEqual(VSCODE_CONTEXT_DEFAULT_LOCAL_FLAGS);
      });
    });

    describe('when an account changes', () => {
      it('updates the instance-level feature flag context but not the local', async () => {
        await featureFlagService.init();
        jest.mocked(vscode.commands.executeCommand).mockClear();

        await triggerOnAccountChange();

        expect(getVSCodeContext()).toEqual(VSCODE_CONTEXT_DEFAULT_INSTANCE_FLAGS);
      });
    });
  });
});

import * as vscode from 'vscode';
import { gql } from 'graphql-request';
import { log } from '../log';
import { CONFIG_NAMESPACE } from '../constants';
import { GraphQLRequest } from '../platform/web_ide';
import { GitLabPlatformManager } from '../platform/gitlab_platform';
import { getExtensionConfiguration } from '../utils/extension_configuration';
import {
  FEATURE_FLAGS_DEFAULT_VALUES,
  FeatureFlag,
  INSTANCE_FEATURE_FLAGS,
  InstanceFeatureFlag,
} from './constants';

export { FeatureFlag, InstanceFeatureFlag, FEATURE_FLAGS_DEFAULT_VALUES } from './constants';

export function isEnabled(feature: FeatureFlag): boolean {
  const featureFlagUserPreferences = getExtensionConfiguration().featureFlags;
  const configurationValue = featureFlagUserPreferences[feature];
  const defaultValue = FEATURE_FLAGS_DEFAULT_VALUES[feature];

  return typeof configurationValue === 'boolean' ? configurationValue : defaultValue;
}

const setFeatureFlagContext = (name: string, enabled: boolean) =>
  vscode.commands.executeCommand('setContext', `gitlab.featureFlags.${name}`, enabled);

const queryGetInstanceFlags = gql`
  query featureFlagsEnabled($names: [String!]!) {
    metadata {
      featureFlags(names: $names) {
        enabled
        name
      }
    }
  }
`;

export type InstanceFeatureFlagsResponseType = {
  metadata?: {
    featureFlags: { enabled: boolean; name: string }[];
  };
};

export const getInstanceFeatureFlagsRequest = (
  featureNames: InstanceFeatureFlag[],
): GraphQLRequest<InstanceFeatureFlagsResponseType> => {
  return {
    type: 'graphql',
    query: queryGetInstanceFlags,
    variables: {
      names: featureNames,
    },
  };
};

export class FeatureFlagService implements vscode.Disposable {
  readonly #manager: GitLabPlatformManager;

  readonly #disposables: vscode.Disposable[] = [];

  constructor(manager: GitLabPlatformManager) {
    this.#manager = manager;

    this.#disposables.push(
      vscode.workspace.onDidChangeConfiguration(async event => {
        if (event.affectsConfiguration(CONFIG_NAMESPACE)) {
          this.#updateLocalFeatureFlags();
        }
      }),
      this.#manager.onAccountChange(async () => {
        await this.#updateInstanceFeatureFlags();
      }),
    );
  }

  async init() {
    this.#updateLocalFeatureFlags();
    await this.#updateInstanceFeatureFlags();
    return this;
  }

  #updateLocalFeatureFlags() {
    Object.values(FeatureFlag).forEach(feature =>
      setFeatureFlagContext(feature, isEnabled(feature)),
    );
  }

  async #updateInstanceFeatureFlags() {
    const values = await this.#fetchInstanceFeatureFlags(INSTANCE_FEATURE_FLAGS);

    return Promise.all(
      INSTANCE_FEATURE_FLAGS.map(flag => {
        const enabled = Boolean(values[flag]);

        return setFeatureFlagContext(flag, enabled);
      }),
    );
  }

  async #fetchInstanceFeatureFlags(flags: InstanceFeatureFlag[]): Promise<Record<string, boolean>> {
    // TODO: We need to check for GitLab instance version 17.4 here
    const platform = await this.#manager.getForActiveAccount(false);

    if (!platform) {
      return {};
    }

    try {
      const response = await platform.fetchFromApi(getInstanceFeatureFlagsRequest(flags));

      if (!response.metadata) {
        return {};
      }

      return Object.fromEntries(
        response.metadata.featureFlags.map(({ name, enabled }) => [name, enabled]),
      );
    } catch (e) {
      log.error(e);

      return {};
    }
  }

  dispose(): void {
    this.#disposables.forEach(ch => ch.dispose());
  }
}

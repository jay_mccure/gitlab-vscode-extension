import vscode from 'vscode';
import { BaseLanguageClient } from 'vscode-languageclient';
import { log } from '../log';
import { createStreamIterator } from './create_stream_iterator';

type IteratorType = ReturnType<typeof createStreamIterator>;

interface CompletionStreamOptions {
  client: BaseLanguageClient;
  streamId: string;
  uniqueTrackingId: string;
  onCancelDetached: (stream: CompletionStream) => void;
}

/** identifies the document and position for this stream */
export const getStreamContextId = (document: vscode.TextDocument, position: vscode.Position) =>
  `${document.uri.toString()}|${position.line}|${position.character}`;

export class CompletionStream {
  #client: BaseLanguageClient;

  #id: string;

  #uniqueTrackingId: string;

  #cancellationTokenSource: vscode.CancellationTokenSource;

  #iterator: IteratorType;

  constructor({ client, streamId, uniqueTrackingId, onCancelDetached }: CompletionStreamOptions) {
    this.#id = streamId;
    this.#uniqueTrackingId = uniqueTrackingId;
    this.#client = client;
    this.#cancellationTokenSource = new vscode.CancellationTokenSource();
    this.#iterator = createStreamIterator(
      this.#client,
      this.#id,
      this.#cancellationTokenSource.token,
      () => onCancelDetached(this),
    );
    log.debug(`Listening to stream ${this.#id}`);
  }

  get iterator() {
    return this.#iterator;
  }

  get trackingId() {
    return this.#uniqueTrackingId;
  }

  cancel() {
    log.debug(`Cancellation requested for stream ${this.#id}`);
    return this.#cancellationTokenSource.cancel();
  }
}

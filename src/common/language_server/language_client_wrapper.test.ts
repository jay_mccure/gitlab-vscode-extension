import vscode, { TabGroup } from 'vscode';
import {
  API_ERROR_NOTIFICATION,
  API_RECOVERY_NOTIFICATION,
  DidChangeDocumentInActiveEditor,
  TRACKING_EVENTS,
} from '@gitlab-org/gitlab-lsp';
import {
  BaseLanguageClient,
  DidChangeConfigurationNotification,
  GenericNotificationHandler,
} from 'vscode-languageclient';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { GitLabPlatformManagerForCodeSuggestions } from '../code_suggestions/gitlab_platform_manager_for_code_suggestions';
import { GitLabPlatformForAccount } from '../platform/gitlab_platform';
import { gitlabPlatformForAccount, gitlabPlatformForProject } from '../test_utils/entities';
import { CodeSuggestionsStateManager } from '../code_suggestions/code_suggestions_state_manager';
import { setFakeWorkspaceConfiguration } from '../test_utils/vscode_fakes';
import { asMutable } from '../test_utils/types';
import { GitLabTelemetryEnvironment } from '../platform/gitlab_telemetry_environment';
import { log } from '../log';
import { LanguageClientWrapper } from './language_client_wrapper';

jest.mock('../code_suggestions/gitlab_platform_manager_for_code_suggestions');
jest.mock('../log'); // disable logging in tests

describe('LanguageClientWrapper', () => {
  let client: BaseLanguageClient;
  const getGitLabPlatformMock = jest.fn();
  const fakeManager = createFakePartial<GitLabPlatformManagerForCodeSuggestions>({
    getGitLabPlatform: getGitLabPlatformMock,
  });

  const gitLabTelemetryEnvironment = createFakePartial<GitLabTelemetryEnvironment>({
    isTelemetryEnabled: jest.fn(),
  });

  let stateManager: CodeSuggestionsStateManager;

  beforeEach(() => {
    const gitLabPlatform: GitLabPlatformForAccount = gitlabPlatformForAccount;
    getGitLabPlatformMock.mockResolvedValue(gitLabPlatform);
    client = createFakePartial<BaseLanguageClient>({
      start: jest.fn(),
      stop: jest.fn(),
      registerProposedFeatures: jest.fn(),
      onNotification: jest.fn(),
      sendNotification: jest.fn(),
    });
    stateManager = createFakePartial<CodeSuggestionsStateManager>({
      setError: jest.fn(),
    });
  });

  describe('initAndStart', () => {
    it('starts the client and synchronizes the configuration', async () => {
      const wrapper = new LanguageClientWrapper(
        client,
        fakeManager,
        stateManager,
        gitLabTelemetryEnvironment,
      );

      await wrapper.initAndStart();

      expect(client.registerProposedFeatures).toHaveBeenCalled();
      expect(client.start).toHaveBeenCalled();
      expect(client.sendNotification).toHaveBeenCalledWith(
        DidChangeConfigurationNotification.type,
        {
          settings: {
            baseUrl: gitlabPlatformForAccount.account.instanceUrl,
            projectPath: '',
            token: gitlabPlatformForAccount.account.token,
            telemetry: {
              actions: [{ action: TRACKING_EVENTS.ACCEPTED }],
            },
            featureFlags: {
              codeSuggestionsClientDirectToGateway: true,
              remoteSecurityScans: false,
              streamCodeGenerations: true,
            },
            codeCompletion: {
              additionalLanguages: [],
              disabledSupportedLanguages: [],
            },
            securityScannerOptions: {
              enabled: false,
            },
            openTabsContext: true,
            suggestionsCache: undefined,
            logLevel: 'info',
            ignoreCertificateErrors: false,
            httpAgentOptions: {
              ca: undefined,
              cert: undefined,
              certKey: undefined,
            },
            duoWorkflowSettings: {
              dockerSocket: '/var/run/docker.sock',
            },
            duo: {
              enabledWithoutGitLabProject: undefined,
            },
          },
        },
      );
    });

    it('stops client when disposed', async () => {
      const wrapper = new LanguageClientWrapper(
        client,
        fakeManager,
        stateManager,
        gitLabTelemetryEnvironment,
      );

      await wrapper.initAndStart();
      wrapper.dispose();

      expect(client.stop).toHaveBeenCalled();
    });

    describe('sendOpenTabs', () => {
      describe('with no open tabs', () => {
        it('does not send and "textDocument/didOpen" events', async () => {
          const wrapper = new LanguageClientWrapper(
            client,
            fakeManager,
            stateManager,
            gitLabTelemetryEnvironment,
          );

          await wrapper.initAndStart();

          expect(client.sendNotification).not.toHaveBeenCalledWith(
            expect.objectContaining({
              method: 'textDocument/didOpen',
            }),
            expect.anything(),
          );
        });
      });

      describe('with tab groups and open tabs', () => {
        beforeEach(() => {
          const mockTextDocuments = [
            createFakePartial<vscode.TextDocument>({
              uri: vscode.Uri.file('/path/to/file1.ts'),
              getText: () => '',
            }),
            createFakePartial<vscode.TextDocument>({
              uri: vscode.Uri.file('/path/to/file2.ts'),
              getText: () => '',
            }),
            createFakePartial<vscode.TextDocument>({
              uri: vscode.Uri.file('/path/to/file3.ts'),
              getText: () => '',
            }),
          ];

          jest.mocked(vscode.workspace.openTextDocument).mockImplementation(uri => {
            let textDocument: vscode.TextDocument | undefined;
            if (uri instanceof vscode.Uri) {
              textDocument = mockTextDocuments.find(doc => doc.uri.path === uri.path);
            } else if (typeof uri === 'string') {
              textDocument = mockTextDocuments.find(doc => doc.uri.path === uri);
            }
            return Promise.resolve(textDocument!);
          });

          asMutable(vscode.window.tabGroups).all = [
            createFakePartial<TabGroup>({
              tabs: [
                createFakePartial<vscode.Tab>({
                  input: new vscode.TabInputText(vscode.Uri.file('/path/to/file1.ts')),
                }),
                createFakePartial<vscode.Tab>({
                  input: new vscode.TabInputText(vscode.Uri.file('/path/to/file2.ts')),
                }),
              ],
            }),
            createFakePartial<TabGroup>({
              tabs: [
                createFakePartial<vscode.Tab>({
                  input: new vscode.TabInputText(vscode.Uri.file('/path/to/file3.ts')),
                }),
                createFakePartial<vscode.Tab>({
                  input: createFakePartial<vscode.TabInputTextDiff>({
                    original: '/path/to/file4.ts',
                  }),
                }),
              ],
            }),
          ];
        });

        afterEach(() => {
          asMutable(vscode.window.tabGroups).all = [];
        });

        it('logs an error when accessing tabs fails', async () => {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          asMutable(vscode.window.tabGroups).all = null as any; // Force an invalid vscode state

          const wrapper = new LanguageClientWrapper(
            client,
            fakeManager,
            stateManager,
            gitLabTelemetryEnvironment,
          );

          await wrapper.initAndStart();

          expect(log.error).toHaveBeenCalledWith(
            'Failed to send existing open tabs to language server: ',
            expect.any(Error),
          );
        });

        it('logs a warning when sending a document fails', async () => {
          const error = new Error(`oh no!`);
          jest.mocked(vscode.workspace.openTextDocument).mockImplementation(uri => {
            if (uri instanceof vscode.Uri && uri.path === '/path/to/file1.ts') {
              throw error;
            }
            return Promise.resolve(createFakePartial<vscode.TextDocument>({}));
          });
          const wrapper = new LanguageClientWrapper(
            client,
            fakeManager,
            stateManager,
            gitLabTelemetryEnvironment,
          );

          await wrapper.initAndStart();

          expect(log.warn).toHaveBeenCalledWith(
            'Failed to send "textDocument.didOpen" event for "file:///path/to/file1.ts"',
            error,
          );
        });

        it('sends "textDocument/didOpen" event for each open document', async () => {
          const wrapper = new LanguageClientWrapper(
            client,
            fakeManager,
            stateManager,
            gitLabTelemetryEnvironment,
          );

          await wrapper.initAndStart();

          expect(client.sendNotification).toHaveBeenCalledWith(
            expect.objectContaining({
              method: 'textDocument/didOpen',
            }),
            { textDocument: expect.objectContaining({ uri: 'file:///path/to/file1.ts' }) },
          );
          expect(client.sendNotification).toHaveBeenCalledWith(
            expect.objectContaining({
              method: 'textDocument/didOpen',
            }),
            { textDocument: expect.objectContaining({ uri: 'file:///path/to/file2.ts' }) },
          );
          expect(client.sendNotification).toHaveBeenCalledWith(
            expect.objectContaining({
              method: 'textDocument/didOpen',
            }),
            { textDocument: expect.objectContaining({ uri: 'file:///path/to/file3.ts' }) },
          );
        });

        it('does not send "textDocument/didOpen" event for unsupported document types', async () => {
          const wrapper = new LanguageClientWrapper(
            client,
            fakeManager,
            stateManager,
            gitLabTelemetryEnvironment,
          );

          await wrapper.initAndStart();

          expect(client.sendNotification).not.toHaveBeenCalledWith(
            expect.objectContaining({
              method: 'textDocument/didOpen',
            }),
            { textDocument: expect.objectContaining({ original: '/path/to/file4.ts' }) },
          );
        });
      });
    });

    describe('sendActiveDocument', () => {
      const activeEditorDocumentFileUri = 'file://file.js';

      beforeEach(async () => {
        vscode.window.activeTextEditor = createFakePartial<vscode.TextEditor>({
          document: { uri: activeEditorDocumentFileUri },
        });

        const wrapper = new LanguageClientWrapper(
          client,
          fakeManager,
          stateManager,
          gitLabTelemetryEnvironment,
        );

        await wrapper.initAndStart();
      });

      it('send DidChangeDocumentInActiveEditor event', async () => {
        expect(client.sendNotification).toHaveBeenCalledWith(
          DidChangeDocumentInActiveEditor,
          activeEditorDocumentFileUri,
        );
      });
    });

    describe('inside a GitLab project', () => {
      beforeEach(() => {
        getGitLabPlatformMock.mockResolvedValue(gitlabPlatformForProject);

        client = createFakePartial<BaseLanguageClient>({
          start: jest.fn(),
          stop: jest.fn(),
          registerProposedFeatures: jest.fn(),
          onNotification: jest.fn(),
          sendNotification: jest.fn(),
        });
        stateManager = createFakePartial<CodeSuggestionsStateManager>({
          setError: jest.fn(),
        });
      });

      it('sends the project path', async () => {
        const wrapper = new LanguageClientWrapper(
          client,
          fakeManager,
          stateManager,
          gitLabTelemetryEnvironment,
        );

        await wrapper.initAndStart();

        expect(client.sendNotification).toHaveBeenCalledWith(
          DidChangeConfigurationNotification.type,
          {
            settings: expect.objectContaining({
              projectPath: gitlabPlatformForProject.project?.namespaceWithPath,
            }),
          },
        );
      });
    });
  });

  describe('sendSuggestionAcceptedEvent', () => {
    it('sends accepted notification', async () => {
      const wrapper = new LanguageClientWrapper(
        client,
        fakeManager,
        stateManager,
        gitLabTelemetryEnvironment,
      );

      // this is important step, we reference the function WITHOUT it's class instance
      // to test that we can pass it around as a command
      const { sendSuggestionAcceptedEvent } = wrapper;

      const trackingId = 'trackingId';
      const optionId = 1;

      await sendSuggestionAcceptedEvent(trackingId, optionId);

      expect(client.sendNotification).toHaveBeenCalledWith('$/gitlab/telemetry', {
        category: 'code_suggestions',
        action: TRACKING_EVENTS.ACCEPTED,
        context: { trackingId, optionId },
      });
    });
  });

  describe('reacts on API errors', () => {
    let notificationHandlers: Record<string, GenericNotificationHandler>;

    let wrapper: LanguageClientWrapper;

    beforeEach(async () => {
      notificationHandlers = {};
      jest.mocked(client.onNotification).mockImplementation((type, handler) => {
        notificationHandlers[type] = handler;
        return { dispose: () => {} };
      });
      wrapper = new LanguageClientWrapper(
        client,
        fakeManager,
        stateManager,
        gitLabTelemetryEnvironment,
      );
      await wrapper.initAndStart();
    });

    it('sets error state when receiving api error notification', async () => {
      notificationHandlers[API_ERROR_NOTIFICATION]();

      expect(stateManager.setError).toHaveBeenLastCalledWith(true);
    });

    it('resets error state when receiving api recovery notification', async () => {
      notificationHandlers[API_RECOVERY_NOTIFICATION]();

      expect(stateManager.setError).toHaveBeenLastCalledWith(false);
    });
  });

  describe('syncConfig', () => {
    let subject: LanguageClientWrapper;

    beforeEach(async () => {
      subject = new LanguageClientWrapper(
        client,
        fakeManager,
        stateManager,
        gitLabTelemetryEnvironment,
      );
      await subject.initAndStart();

      // initAndStart() will trigger some mocks, so let's start from a clean slate
      jest.clearAllMocks();
    });

    it('reads featureFlags configuration', async () => {
      setFakeWorkspaceConfiguration({
        featureFlags: {
          codeSuggestionsClientDirectToGateway: true,
          streamCodeGenerations: false,
          // Include somethingElse to show that it is not included
          somethingElse: false,
        },
      });

      await subject.syncConfig();

      expect(client.sendNotification).toHaveBeenCalledTimes(1);
      expect(client.sendNotification).toHaveBeenCalledWith(
        DidChangeConfigurationNotification.type,
        {
          settings: expect.objectContaining({
            featureFlags: {
              codeSuggestionsClientDirectToGateway: true,
              remoteSecurityScans: false,
              streamCodeGenerations: false,
            },
          }),
        },
      );
    });

    describe('syncs telemetry configuration', () => {
      it.each([true, false])('when telemetry enabled is set to %s', async isTelemetryEnabled => {
        jest
          .mocked(gitLabTelemetryEnvironment.isTelemetryEnabled)
          .mockReturnValueOnce(isTelemetryEnabled);

        await subject.syncConfig();

        expect(client.sendNotification).toHaveBeenCalledWith(
          DidChangeConfigurationNotification.type,
          {
            settings: expect.objectContaining({
              telemetry: expect.objectContaining({
                enabled: isTelemetryEnabled,
              }),
            }),
          },
        );
      });
    });

    describe.each`
      configured                                | expected
      ${{ ignoreCertificateErrors: false }}     | ${{ ignoreCertificateErrors: false }}
      ${{ ignoreCertificateErrors: null }}      | ${{ ignoreCertificateErrors: false }}
      ${{ ignoreCertificateErrors: true }}      | ${{ ignoreCertificateErrors: true }}
      ${{ ignoreCertificateErrors: undefined }} | ${{ ignoreCertificateErrors: false }}
      ${{ ca: 'test-ca' }}                      | ${{ httpAgentOptions: { ca: 'test-ca' } }}
      ${{ cert: 'test-cert' }}                  | ${{ httpAgentOptions: { cert: 'test-cert' } }}
      ${{ certKey: 'test-certKey' }}            | ${{ httpAgentOptions: { certKey: 'test-certKey' } }}
    `('$expected when workspace included $configured', ({ configured, expected }) => {
      it('should send a configuration changed notification', async () => {
        setFakeWorkspaceConfiguration(configured);

        await subject.syncConfig();

        expect(client.sendNotification).toHaveBeenCalledWith(
          DidChangeConfigurationNotification.type,
          {
            settings: expect.objectContaining(expected),
          },
        );
      });
    });
  });
});

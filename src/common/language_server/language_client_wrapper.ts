import vscode from 'vscode';
import {
  API_ERROR_NOTIFICATION,
  API_RECOVERY_NOTIFICATION,
  ClientConfig,
  TelemetryNotificationType,
  TRACKING_EVENTS,
  CODE_SUGGESTIONS_CATEGORY,
  TokenCheckNotificationType,
  TokenCheckNotificationParams,
  DidChangeDocumentInActiveEditor,
} from '@gitlab-org/gitlab-lsp';
import {
  BaseLanguageClient,
  DidChangeConfigurationNotification,
  DidOpenTextDocumentNotification,
} from 'vscode-languageclient';
import { GitLabPlatformManagerForCodeSuggestions } from '../code_suggestions/gitlab_platform_manager_for_code_suggestions';
import { CodeSuggestionsStateManager } from '../code_suggestions/code_suggestions_state_manager';
import {
  getExtensionConfiguration,
  getDuoCodeSuggestionsConfiguration,
  getHttpAgentConfiguration,
  parseDisabledSupportedLanguages,
  getSecurityScannerConfiguration,
} from '../utils/extension_configuration';
import { log } from '../log';
import { FeatureFlag, isEnabled } from '../feature_flags/feature_flag_service';
import { GitLabTelemetryEnvironment } from '../platform/gitlab_telemetry_environment';

export class LanguageClientWrapper {
  #client: BaseLanguageClient;

  #suggestionsManager: GitLabPlatformManagerForCodeSuggestions;

  #stateManager: CodeSuggestionsStateManager;

  #telemetryEnvironment: GitLabTelemetryEnvironment;

  #subscriptions: vscode.Disposable[] = [];

  constructor(
    client: BaseLanguageClient,
    suggestionsManager: GitLabPlatformManagerForCodeSuggestions,
    stateManager: CodeSuggestionsStateManager,
    telemetryEnvironment: GitLabTelemetryEnvironment,
  ) {
    this.#client = client;
    this.#suggestionsManager = suggestionsManager;
    this.#stateManager = stateManager;
    this.#telemetryEnvironment = telemetryEnvironment;
  }

  async initAndStart() {
    this.#client.registerProposedFeatures();
    this.#subscriptions.push();

    this.#client.onNotification(
      TokenCheckNotificationType,
      (response: TokenCheckNotificationParams) => {
        log.warn(
          `Token validation failed in Language Server: (${response.message}). This can happen with OAuth token refresh. If the rest of the extension works, this won't be a problem.`,
        );
      },
    );

    this.#client.onNotification('$/gitlab/openUrl', ({ url }) =>
      vscode.env.openExternal(url).then(result => {
        if (!result) {
          log.warn(`Unable to open URL: ${url}`);
        }

        return result;
      }),
    );

    this.#client.onNotification(API_ERROR_NOTIFICATION, () => this.#stateManager.setError(true));
    this.#client.onNotification(API_RECOVERY_NOTIFICATION, () =>
      this.#stateManager.setError(false),
    );
    await this.#client.start();
    this.#subscriptions.push({ dispose: () => this.#client.stop() });
    await this.syncConfig();
    await this.#sendOpenTabs();
    await this.#sendActiveDocument();
  }

  syncConfig = async () => {
    const platform = await this.#suggestionsManager.getGitLabPlatform();
    if (!platform) {
      log.warn('There is no GitLab account available with access to suggestions');
      return;
    }
    const extensionConfiguration = getExtensionConfiguration();
    const httpAgentConfiguration = getHttpAgentConfiguration();
    const codeSuggestionsConfiguration = getDuoCodeSuggestionsConfiguration();
    const securityScannerConfiguration = getSecurityScannerConfiguration();
    const settings: ClientConfig = {
      baseUrl: platform.account.instanceUrl,
      token: platform.account.token,
      telemetry: {
        trackingUrl: extensionConfiguration.trackingUrl,
        enabled: this.#telemetryEnvironment.isTelemetryEnabled(),
        actions: [{ action: TRACKING_EVENTS.ACCEPTED }],
      },
      featureFlags: {
        [FeatureFlag.CodeSuggestionsClientDirectToGateway]: isEnabled(
          FeatureFlag.CodeSuggestionsClientDirectToGateway,
        ),
        [FeatureFlag.StreamCodeGenerations]: isEnabled(FeatureFlag.StreamCodeGenerations),
        [FeatureFlag.RemoteSecurityScans]: isEnabled(FeatureFlag.RemoteSecurityScans),
      },
      openTabsContext: codeSuggestionsConfiguration.openTabsContext,
      suggestionsCache: codeSuggestionsConfiguration.suggestionsCache,
      codeCompletion: {
        additionalLanguages: codeSuggestionsConfiguration.additionalLanguages,
        disabledSupportedLanguages: parseDisabledSupportedLanguages(
          codeSuggestionsConfiguration.enabledSupportedLanguages,
        ),
      },
      logLevel: extensionConfiguration.debug ? 'debug' : 'info',
      projectPath: platform.project?.namespaceWithPath ?? '',
      ignoreCertificateErrors: extensionConfiguration.ignoreCertificateErrors,
      httpAgentOptions: {
        ...httpAgentConfiguration,
      },
      securityScannerOptions: {
        enabled: securityScannerConfiguration.enabled,
      },
      duoWorkflowSettings: {
        dockerSocket: extensionConfiguration.dockerSocket,
      },
      duo: {
        enabledWithoutGitlabProject: extensionConfiguration.duo.enabledWithoutGitLabProject,
      },
    };

    log.info(`Configuring Language Server - baseUrl: ${platform.account.instanceUrl}`);
    await this.#client.sendNotification(DidChangeConfigurationNotification.type, {
      settings,
    });
  };

  sendSuggestionAcceptedEvent = async (trackingId: string, optionId?: number) =>
    this.#client.sendNotification(TelemetryNotificationType.method, {
      category: CODE_SUGGESTIONS_CATEGORY,
      action: TRACKING_EVENTS.ACCEPTED,
      context: { trackingId, optionId },
    });

  async #sendOpenTabs() {
    try {
      const didOpenEvents = vscode.window.tabGroups.all.flatMap(tabGroup =>
        tabGroup.tabs
          .filter(tab => tab.input instanceof vscode.TabInputText)
          .map(tab => {
            const input = tab.input as vscode.TabInputText;

            return this.#sendDidOpenTextDocumentEvent(input.uri).catch(error => {
              log.warn(
                `Failed to send "textDocument.didOpen" event for "${input.uri.toString()}"`,
                error,
              );
              return null;
            });
          }),
      );

      log.debug(
        `Sending ${didOpenEvents.filter(Boolean).length} existing open text documents to language server`,
      );
      await Promise.all(didOpenEvents);
    } catch (error) {
      log.error('Failed to send existing open tabs to language server: ', error);
    }
  }

  async #sendActiveDocument() {
    if (vscode.window.activeTextEditor) {
      await this.#client.sendNotification(
        DidChangeDocumentInActiveEditor,
        vscode.window.activeTextEditor.document.uri.toString(),
      );
    }
  }

  async #sendDidOpenTextDocumentEvent(uri: vscode.Uri) {
    const textDocument = await vscode.workspace.openTextDocument(uri);

    return this.#client.sendNotification(DidOpenTextDocumentNotification.type, {
      textDocument: {
        uri: textDocument.uri.toString(),
        languageId: textDocument.languageId,
        version: textDocument.version,
        text: textDocument.getText(),
      },
    });
  }

  dispose = () => {
    this.#subscriptions.forEach(s => s.dispose());
  };
}

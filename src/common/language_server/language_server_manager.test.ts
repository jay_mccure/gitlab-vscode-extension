import vscode from 'vscode';
import { BaseLanguageClient } from 'vscode-languageclient';
import {
  DidChangeDocumentInActiveEditor,
  SUGGESTION_ACCEPTED_COMMAND,
} from '@gitlab-org/gitlab-lsp';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { createExtensionContext } from '../test_utils/entities';
import {
  createConfigurationChangeTrigger,
  createActiveTextEditorChangeTrigger,
} from '../test_utils/vscode_fakes';
import { COMMAND_TOGGLE_CODE_SUGGESTIONS } from '../code_suggestions/commands/toggle';
import { SHOW_QUICK_PICK_MENU } from '../duo_quick_pick/commands/show_quick_pick_menu';
import { CodeSuggestionsStateManager } from '../code_suggestions/code_suggestions_state_manager';
import { CodeSuggestionsGutterIcon } from '../code_suggestions/code_suggestions_gutter_icon';
import { CODE_SUGGESTION_STREAM_ACCEPTED_COMMAND } from '../code_suggestions/commands/code_suggestion_stream_accepted';
import { GitLabPlatformManagerForCodeSuggestions } from '../code_suggestions/gitlab_platform_manager_for_code_suggestions';
import { DependencyContainer } from '../dependency_container';
import { LanguageClientWrapper } from './language_client_wrapper';
import { LanguageServerManager } from './language_server_manager';
import { LanguageClientFactory } from './client_factory';

jest.mock('../code_suggestions/code_suggestions_gutter_icon');
jest.mock('../code_suggestions/code_suggestions_status_bar_item');
jest.mock('../code_suggestions/code_suggestions_state_manager');
jest.mock('./language_client_wrapper');
jest.mock('../code_suggestions/gitlab_platform_manager_for_code_suggestions');

describe('LanguageServerManager', () => {
  let triggerConfigChange: () => void;
  let triggerTelemetryChange: (enabled: boolean) => void;
  let clientWrapper: LanguageClientWrapper;
  let platformManager: GitLabPlatformManagerForCodeSuggestions;
  let client: BaseLanguageClient;
  let languageClientFactory: LanguageClientFactory;
  let dependencyContainer: DependencyContainer;
  let stateManager: CodeSuggestionsStateManager;
  let context: vscode.ExtensionContext;
  let languageServerManager: LanguageServerManager;
  const triggerActiveTextEditorChange = createActiveTextEditorChangeTrigger();

  beforeEach(async () => {
    triggerConfigChange = createConfigurationChangeTrigger();
    clientWrapper = createFakePartial<LanguageClientWrapper>({
      initAndStart: jest.fn(),
      sendSuggestionAcceptedEvent: jest.fn(),
      syncConfig: jest.fn(),
      dispose: jest.fn(),
    });

    platformManager = createFakePartial<GitLabPlatformManagerForCodeSuggestions>({
      getGitLabPlatform: jest.fn(),
      onAccountChange: jest.fn(),
      dispose: jest.fn(),
    });
    jest.mocked(LanguageClientWrapper).mockReturnValue(clientWrapper);
    jest.mocked(GitLabPlatformManagerForCodeSuggestions).mockReturnValue(platformManager);

    client = createFakePartial<BaseLanguageClient>({
      stop: jest.fn(),
      sendNotification: jest.fn(),
      sendRequest: jest.fn(),
    });

    languageClientFactory = createFakePartial<LanguageClientFactory>({
      createLanguageClient: jest.fn(() => client),
    });
    dependencyContainer = createFakePartial<DependencyContainer>({
      gitLabPlatformManager: {
        onAccountChange: jest.fn().mockReturnValue({
          dispose: jest.fn(),
        }),
      },
      gitLabTelemetryEnvironment: {
        isTelemetryEnabled: jest.fn(),
        onDidChangeTelemetryEnabled: jest.fn().mockImplementation(trigger => {
          triggerTelemetryChange = trigger;
          return {
            dispose: jest.fn(),
          };
        }),
      },
    });
    stateManager = createFakePartial<CodeSuggestionsStateManager>({
      init: jest.fn(),
      onDidChangeVisibleState: jest.fn(),
    });
    context = createExtensionContext();
    jest.mocked(CodeSuggestionsStateManager).mockReturnValue(stateManager);

    languageServerManager = new LanguageServerManager(
      context,
      languageClientFactory,
      dependencyContainer,
    );
    await languageServerManager.startLanguageServer();
  });

  it('does not do anything if language server is already started', async () => {
    await languageServerManager.startLanguageServer();
    expect(languageClientFactory.createLanguageClient).toHaveBeenCalledTimes(1);
  });

  it('creates a language client and provides a baseAssetsUrl', () => {
    expect(languageClientFactory.createLanguageClient).toHaveBeenCalledWith(
      context,
      expect.objectContaining({
        initializationOptions: expect.objectContaining({
          baseAssetsUrl: expect.stringContaining('/assets/language-server/'),
        }),
      }),
    );
  });

  it('initializes the language client wrapper', async () => {
    expect(clientWrapper.initAndStart).toHaveBeenCalled();
  });

  it('initializes state manager', async () => {
    expect(stateManager.init).toHaveBeenCalled();
  });

  it('registers suggestion accepted command', () => {
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      SUGGESTION_ACCEPTED_COMMAND,
      clientWrapper.sendSuggestionAcceptedEvent,
    );
  });

  it('registers streamed suggestion accepted command', () => {
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      CODE_SUGGESTION_STREAM_ACCEPTED_COMMAND,
      expect.any(Function),
    );
  });

  it('registers the command to toggle code suggestions on/off', () => {
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_TOGGLE_CODE_SUGGESTIONS,
      expect.any(Function),
    );
  });

  it('registers the command to show the quick pick menu', () => {
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      SHOW_QUICK_PICK_MENU,
      expect.any(Function),
    );
  });

  it('registers configuration change listener', () => {
    triggerConfigChange();
    expect(clientWrapper.syncConfig).toHaveBeenCalled();
  });

  it('registers telemetry change listener', () => {
    triggerTelemetryChange(true);
    expect(clientWrapper.syncConfig).toHaveBeenCalled();
  });

  it('registers on account change listener', () => {
    expect(dependencyContainer.gitLabPlatformManager.onAccountChange).toHaveBeenCalledWith(
      clientWrapper.syncConfig,
    );
  });

  it('registers  change document in active editor', async () => {
    const uri = 'file://file.js';
    await triggerActiveTextEditorChange(
      createFakePartial<vscode.TextEditor>({ document: { uri } }),
    );
    expect(client.sendNotification).toHaveBeenCalledWith(DidChangeDocumentInActiveEditor, uri);
  });

  it('creates CodeSuggestionsGutterIcon', () => {
    expect(CodeSuggestionsGutterIcon).toHaveBeenCalledTimes(1);
    expect(CodeSuggestionsGutterIcon).toHaveBeenCalledWith(context, stateManager);
  });

  describe('restartLanguageServer', () => {
    beforeEach(async () => {
      await languageServerManager.restartLanguageServer();
    });

    it('stops the existing client', () => {
      expect(client.stop).toHaveBeenCalled();
    });

    it('disposes the existing wrapper', () => {
      expect(clientWrapper.dispose).toHaveBeenCalled();
    });

    it('reinitializes the language server', () => {
      expect(languageClientFactory.createLanguageClient).toHaveBeenCalledTimes(2);
      expect(clientWrapper.initAndStart).toHaveBeenCalledTimes(2);
    });
  });

  describe('sendRequest and sendNotification with uninitialized client', () => {
    let uninitializedLanguageServerManager: LanguageServerManager;

    beforeEach(() => {
      uninitializedLanguageServerManager = new LanguageServerManager(
        context,
        languageClientFactory,
        dependencyContainer,
      );
    });

    it('sendRequest returns undefined if client is not initialized', async () => {
      const result = await uninitializedLanguageServerManager.sendRequest('testMethod');
      expect(result).toBeUndefined();
    });

    it('sendNotification returns undefined if client is not initialized', async () => {
      const result = await uninitializedLanguageServerManager.sendNotification('testNotification');
      expect(result).toBeUndefined();
    });
  });

  describe('sendRequest', () => {
    it('sends request to the client if initialized', async () => {
      const mockResponse = { data: 'test' };
      jest.mocked(client.sendRequest).mockResolvedValue(mockResponse);

      const result = await languageServerManager.sendRequest('testMethod', { param: 'value' });

      expect(client.sendRequest).toHaveBeenCalledWith('testMethod', { param: 'value' }, undefined);
      expect(result).toEqual(mockResponse);
    });

    it('passes cancellation token to the client', async () => {
      const mockToken = {} as vscode.CancellationToken;
      await languageServerManager.sendRequest('testMethod', { param: 'value' }, mockToken);

      expect(client.sendRequest).toHaveBeenCalledWith('testMethod', { param: 'value' }, mockToken);
    });
  });

  describe('sendNotification', () => {
    it('sends notification to the client if initialized', async () => {
      jest.mocked(client.sendNotification).mockResolvedValue(undefined);

      const result = await languageServerManager.sendNotification('testNotification', {
        param: 'value',
      });

      expect(client.sendNotification).toHaveBeenCalledWith('testNotification', { param: 'value' });
      expect(result).toBe(true);
    });
  });
});

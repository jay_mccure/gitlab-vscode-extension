import {
  DidChangeDocumentInActiveEditor,
  SUGGESTION_ACCEPTED_COMMAND,
} from '@gitlab-org/gitlab-lsp';
import vscode from 'vscode';
import { BaseLanguageClient, CancellationToken } from 'vscode-languageclient';
import { CodeSuggestionsGutterIcon } from '../code_suggestions/code_suggestions_gutter_icon';
import { CodeSuggestionsStateManager } from '../code_suggestions/code_suggestions_state_manager';
import { CodeSuggestionsStatusBarItem } from '../code_suggestions/code_suggestions_status_bar_item';
import {
  CODE_SUGGESTION_STREAM_ACCEPTED_COMMAND,
  codeSuggestionStreamAccepted,
} from '../code_suggestions/commands/code_suggestion_stream_accepted';
import {
  COMMAND_TOGGLE_CODE_SUGGESTIONS,
  toggleCodeSuggestions,
} from '../code_suggestions/commands/toggle';
import { COMMAND_RUN_SECURITY_SCAN, runSecurityScan } from '../security_scans/run_security_scan';
import { GitLabPlatformManagerForCodeSuggestions } from '../code_suggestions/gitlab_platform_manager_for_code_suggestions';
import { CONFIG_NAMESPACE } from '../constants';
import { DependencyContainer } from '../dependency_container';
import { log } from '../log';
import {
  SHOW_QUICK_PICK_MENU,
  showDuoQuickPickMenu,
} from '../duo_quick_pick/commands/show_quick_pick_menu';
import { WebviewInfo, WebviewInfoProvider } from '../webview/webview_info_provider';
import { WebviewThemePublisher, Theme } from '../webview/theme/types';
import { LanguageClientFactory } from './client_factory';
import { getClientContext } from './get_client_context';
import { LanguageClientMiddleware } from './language_client_middleware';
import { LanguageClientWrapper } from './language_client_wrapper';

export class LanguageServerManager implements WebviewInfoProvider, WebviewThemePublisher {
  #client: BaseLanguageClient | undefined;

  #wrapper: LanguageClientWrapper | undefined;

  #context: vscode.ExtensionContext;

  #dependencyContainer: DependencyContainer;

  #clientFactory: LanguageClientFactory;

  #subscriptions: vscode.Disposable[] = [];

  constructor(
    context: vscode.ExtensionContext,
    clientFactory: LanguageClientFactory,
    dependencyContainer: DependencyContainer,
  ) {
    this.#context = context;
    this.#clientFactory = clientFactory;
    this.#dependencyContainer = dependencyContainer;
  }

  async startLanguageServer() {
    if (this.#client) {
      log.warn('Language server already started');
      return;
    }
    const { gitLabPlatformManager, gitLabTelemetryEnvironment } = this.#dependencyContainer;
    const stateManager = new CodeSuggestionsStateManager(gitLabPlatformManager, this.#context);
    const statusBarItem = new CodeSuggestionsStatusBarItem(stateManager);
    const gutterIcon = new CodeSuggestionsGutterIcon(this.#context, stateManager);
    const middleware = new LanguageClientMiddleware(stateManager);
    const baseAssetsUrl = vscode.Uri.joinPath(
      this.#context.extensionUri,
      './assets/language-server/',
    ).toString();

    this.#client = this.#clientFactory.createLanguageClient(this.#context, {
      documentSelector: [
        { scheme: 'file' },
        { notebook: '*' },
        { scheme: 'gitlab-web-ide' },
        { scheme: 'untitled' },
      ],
      initializationOptions: {
        ...getClientContext(),
        baseAssetsUrl,
      },
      middleware,
    });

    middleware.client = this.#client;
    stateManager.client = this.#client;
    await stateManager.init();

    const suggestionsManager = new GitLabPlatformManagerForCodeSuggestions(gitLabPlatformManager);
    this.#wrapper = new LanguageClientWrapper(
      this.#client,
      suggestionsManager,
      stateManager,
      gitLabTelemetryEnvironment,
    );

    await this.#wrapper.initAndStart();
    const subscriptions = [
      suggestionsManager,
      this.#wrapper,
      vscode.commands.registerCommand(
        SUGGESTION_ACCEPTED_COMMAND,
        this.#wrapper.sendSuggestionAcceptedEvent,
      ),
      vscode.commands.registerCommand(COMMAND_TOGGLE_CODE_SUGGESTIONS, () =>
        toggleCodeSuggestions({ stateManager }),
      ),
      vscode.commands.registerCommand(SHOW_QUICK_PICK_MENU, () =>
        showDuoQuickPickMenu({ stateManager }),
      ),
      vscode.commands.registerCommand(
        CODE_SUGGESTION_STREAM_ACCEPTED_COMMAND,
        codeSuggestionStreamAccepted(this.#client),
      ),
      vscode.commands.registerCommand(COMMAND_RUN_SECURITY_SCAN, runSecurityScan(this.#client)),
      vscode.workspace.onDidChangeConfiguration(async e => {
        if (!e.affectsConfiguration(CONFIG_NAMESPACE)) {
          return;
        }

        await this.#wrapper?.syncConfig();
      }),
      gitLabTelemetryEnvironment.onDidChangeTelemetryEnabled(this.#wrapper.syncConfig),
      gitLabPlatformManager.onAccountChange(this.#wrapper.syncConfig),

      statusBarItem,
      gutterIcon,
      vscode.window.onDidChangeActiveTextEditor(async te => {
        if (te) {
          await this.#client?.sendNotification(
            DidChangeDocumentInActiveEditor,
            te.document.uri.toString(),
          );
        }
      }),
    ];
    this.#context.subscriptions.push(...subscriptions);
    this.#subscriptions = subscriptions;
  }

  getWebviewInfos(): Promise<WebviewInfo[]> {
    if (!this.#client)
      throw new Error(
        'Language Server client is not initialized. The manager cannot provide webview info',
      );
    return this.#client.sendRequest<WebviewInfo[]>('$/gitlab/webview-metadata');
  }

  publishWebviewTheme(theme: Theme): Promise<void> {
    if (!this.#client) {
      throw new Error('Language Server client is not initialized. Cannot publish webview theme.');
    }

    return this.#client.sendNotification('$/gitlab/theme/didChangeTheme', theme);
  }

  async restartLanguageServer() {
    if (this.#client) {
      await this.#client.stop();
      this.#client = undefined;
      this.#wrapper?.dispose();
    }
    for (const subscription of this.#subscriptions) {
      subscription.dispose();
    }
    this.#subscriptions = [];
    await this.startLanguageServer();
  }

  /**
   * Sends a request to the language server.
   * Returns undefined if the client is not initialized.
   */
  async sendRequest<R>(
    method: string,
    param?: unknown,
    token?: CancellationToken,
  ): Promise<undefined | R> {
    if (!this.#client) {
      return undefined;
    }
    return this.#client.sendRequest<R>(method, param, token);
  }

  /**
   * Sends a notification to the language server.
   * Returns undefined if the client is not initialized.
   */
  async sendNotification(method: string, param?: unknown): Promise<undefined | true> {
    if (!this.#client) {
      return undefined;
    }
    await this.#client.sendNotification(method, param);
    return true;
  }
}

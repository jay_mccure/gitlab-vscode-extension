import * as vscode from 'vscode';
import { FeatureFlagService } from './feature_flags/feature_flag_service';
import {
  COMMAND_CODE_SUGGESTION_ACCEPTED,
  codeSuggestionAccepted,
} from './code_suggestions/commands/code_suggestion_accepted';
import {
  COMMAND_TOGGLE_CODE_SUGGESTIONS_FOR_LANGUAGE,
  toggleCodeSuggestionsForLanguage,
} from './code_suggestions/commands/toggle_language';
import { COMMAND_SHOW_OUTPUT, createShowOutputCommand } from './show_output_command';
import { activateChat } from './chat/gitlab_chat';
import { setupTelemetry } from './snowplow/setup_telemetry';
import { DependencyContainer } from './dependency_container';
import { setupVersionCheck } from './gitlab/check_version';
import { AIContextManager } from './chat/ai_context_manager';

export const activateCommon = async (
  context: vscode.ExtensionContext,
  container: DependencyContainer,
  outputChannel: vscode.OutputChannel,
  aiContextManager: AIContextManager,
) => {
  setupTelemetry(context, container.gitLabTelemetryEnvironment);

  const featureFlagService = new FeatureFlagService(container.gitLabPlatformManager);

  context.subscriptions.push(
    setupVersionCheck(container.gitLabPlatformManager, context),
    await featureFlagService.init(),
  );

  const commands = {
    [COMMAND_SHOW_OUTPUT]: createShowOutputCommand(outputChannel),
    [COMMAND_CODE_SUGGESTION_ACCEPTED]: codeSuggestionAccepted,
    [COMMAND_TOGGLE_CODE_SUGGESTIONS_FOR_LANGUAGE]: toggleCodeSuggestionsForLanguage,
  };
  Object.entries(commands).forEach(([cmdName, cmd]) => {
    context.subscriptions.push(vscode.commands.registerCommand(cmdName, cmd));
  });

  await activateChat(context, container.gitLabPlatformManager, aiContextManager);
};

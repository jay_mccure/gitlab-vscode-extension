export const COMMAND_OPEN_QUICK_CHAT = 'gl.openQuickChat';
// we cannot update the command action label dynamically,
// instead we can register two identical commands that will execute
// based on the platform variable.
// That's why we have 2 commands to send quick chat
export const COMMAND_SEND_QUICK_CHAT = 'gl.sendQuickChat';
export const COMMAND_SEND_QUICK_CHAT_DUPLICATE = 'gl.sendQuickChatDup';
export const COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT = 'gl.copyCodeSnippetFromQuickChat';

import vscode from 'vscode';
import { Cable } from '@anycable/core';
import { SPECIAL_MESSAGES } from '../chat/constants';
import { GitLabPlatformManagerForChat } from '../chat/get_platform_manager_for_chat';
import { AIContextManager } from '../chat/ai_context_manager';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { GitLabChatApi } from '../chat/gitlab_chat_api';
import {
  createFakeWorkspaceConfiguration,
  createConfigurationChangeTrigger,
} from '../test_utils/vscode_fakes';
import { QuickChat } from './quick_chat';
import * as utils from './utils';
import { COMMENT_CONTROLLER_ID, generateThreadLabel } from './utils';

jest.mock('../chat/gitlab_chat_api');

const mockCommentController = createFakePartial<vscode.CommentController>({
  createCommentThread: jest.fn(),
});

jest.mock('./utils', () => {
  const originalModule = jest.requireActual('./utils');
  const mockModule = jest.createMockFromModule<typeof originalModule>('./utils');

  return {
    ...mockModule,
    MarkdownProcessorPipeline: jest.fn().mockImplementation(() => ({
      process: jest.fn(md => md),
    })),
    createCommentController: jest.fn(() => mockCommentController),
    generateThreadLabel: jest.fn(() => 'Duo Quick Chat'),
  };
});

describe('QuickChat', () => {
  let quickChat: QuickChat;
  let mockManager: GitLabPlatformManagerForChat;
  let mockApi: jest.Mocked<GitLabChatApi>;
  let mockConfig: vscode.WorkspaceConfiguration;
  let mockThread: vscode.CommentThread;
  let mockAiContextManager: AIContextManager;
  let mockExtensionContext: vscode.ExtensionContext;

  beforeEach(() => {
    mockManager = createFakePartial<GitLabPlatformManagerForChat>({
      getProjectGqlId: jest.fn().mockResolvedValue('mock-project-id'),
      getGitLabPlatform: jest.fn().mockResolvedValue(undefined),
      getGitLabEnvironment: jest.fn().mockResolvedValue(undefined),
    });

    mockApi = {
      clearChat: jest.fn().mockResolvedValue({}),
      resetChat: jest.fn().mockResolvedValue({}),
      subscribeToUpdates: jest.fn(),
      processNewUserPrompt: jest.fn(),
    } as unknown as jest.Mocked<GitLabChatApi>;

    (GitLabChatApi as jest.MockedClass<typeof GitLabChatApi>).mockImplementation(() => mockApi);

    mockConfig = createFakeWorkspaceConfiguration({
      keybindingHints: { enabled: true },
    });
    jest.spyOn(vscode.workspace, 'getConfiguration').mockReturnValue(mockConfig);

    mockThread = createFakePartial<vscode.CommentThread>({
      comments: [],
      range: new vscode.Range(0, 0, 0, 0),
    });
    mockAiContextManager = createFakePartial<AIContextManager>({});
    mockExtensionContext = createFakePartial<vscode.ExtensionContext>({});
    quickChat = new QuickChat(mockManager, mockExtensionContext, mockAiContextManager);
  });

  const triggerTextEditorSelectionChange = (e: vscode.TextEditorSelectionChangeEvent) => {
    jest
      .mocked(vscode.window.onDidChangeTextEditorSelection)
      .mock.calls.forEach(([listener]) => listener(e));
  };

  async function showChatWithEditor(userInput = 'Test question') {
    const mockUri = vscode.Uri.file('/path/to/file.ts');
    const mockPosition = new vscode.Position(0, 0);
    const mockRange = new vscode.Range(mockPosition, mockPosition);
    const mockEditor = createFakePartial<vscode.TextEditor>({
      document: { uri: mockUri },
      selection: mockRange,
      setDecorations: jest.fn(),
    });
    (vscode.window.activeTextEditor as unknown) = mockEditor;

    (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);

    jest.spyOn(vscode.window, 'showInputBox').mockResolvedValue(userInput);

    await quickChat.show();

    return { mockUri, mockPosition, mockRange, mockEditor };
  }

  describe('show', () => {
    it('creates a new comment thread when an editor is active', async () => {
      const { mockUri, mockRange } = await showChatWithEditor();

      expect(mockApi.clearChat).toHaveBeenCalled();
      expect(utils.setLoadingContext).toHaveBeenCalledWith(false);
      expect(mockCommentController.createCommentThread).toHaveBeenCalledWith(
        mockUri,
        mockRange,
        [],
      );
      expect(mockThread.collapsibleState).toBe(vscode.CommentThreadCollapsibleState.Expanded);
      expect(mockThread.label).toBe('Duo Quick Chat');
    });

    it('does nothing when no editor is active', async () => {
      (vscode.window.activeTextEditor as unknown) = undefined;

      await quickChat.show();

      expect(mockApi.clearChat).not.toHaveBeenCalled();
    });

    it('sends user input when provided through input box', async () => {
      jest.spyOn(quickChat, 'send');
      const userInput = 'This is a test question';
      await showChatWithEditor(userInput);

      expect(vscode.window.showInputBox).toHaveBeenCalledWith({
        placeHolder: 'Ask a question or give an instruction...',
        title: 'GitLab Duo Quick Chat',
      });

      expect(quickChat.send).toHaveBeenCalledWith({ text: userInput, thread: mockThread });
    });
  });

  describe('send', () => {
    let mockReply: vscode.CommentReply;
    let updateHandler: jest.Mock;

    beforeEach(() => {
      mockReply = createFakePartial<vscode.CommentReply>({
        thread: mockThread,
        text: 'Test question',
      });

      updateHandler = jest.fn();
      mockApi.subscribeToUpdates.mockImplementation(handler => {
        updateHandler.mockImplementation(handler);
        return Promise.resolve({} as Cable);
      });

      (utils.createComment as jest.Mock).mockImplementation(text => ({ body: text }));

      return showChatWithEditor();
    });

    it('handles full response correctly', async () => {
      const responseText = 'Full response text';
      await updateHandler({ content: responseText });

      expect(mockThread.comments[2].body).toEqual(expect.objectContaining({ value: responseText }));
    });

    it('handles chunked responses correctly', async () => {
      const mockAppendMarkdown = jest.fn();
      const mockMarkdown = {
        appendMarkdown: mockAppendMarkdown,
        value: '',
      };
      (vscode.MarkdownString as jest.Mock) = jest.fn(() => mockMarkdown);

      await quickChat.send(mockReply);
      await updateHandler({ chunkId: 1, content: 'Chunk 1 ' });
      await updateHandler({ chunkId: 2, content: 'Chunk 2 ' });

      expect(mockAppendMarkdown).toHaveBeenNthCalledWith(1, 'Chunk 1 ');
      expect(mockAppendMarkdown).toHaveBeenNthCalledWith(2, 'Chunk 2 ');
    });

    it('handles /clear command correctly', async () => {
      mockReply.text = SPECIAL_MESSAGES.CLEAR;
      await quickChat.send(mockReply);

      expect(mockApi.clearChat).toHaveBeenCalled();
      expect(utils.setLoadingContext).toHaveBeenCalledWith(false);
      expect(mockThread.comments).toEqual([]);
    });

    it('handles /reset command correctly', async () => {
      jest
        .mocked(utils.createChatResetComment)
        .mockReturnValue(createFakePartial<vscode.Comment>({ body: 'reset' }));

      mockReply.text = SPECIAL_MESSAGES.RESET;
      await quickChat.send(mockReply);

      expect(mockApi.resetChat).toHaveBeenCalled();
      expect(utils.setLoadingContext).toHaveBeenCalledWith(false);
      expect(mockThread.comments).toContainEqual({ body: 'reset' });
    });
  });

  describe('Quick Actions', () => {
    it('registers a completion item provider for quick actions', () => {
      expect(vscode.languages.registerCompletionItemProvider).toHaveBeenCalledWith(
        { scheme: 'comment', pattern: '**' },
        { provideCompletionItems: utils.provideCompletionItems },
        '/',
      );
    });
  });

  describe('thread label update', () => {
    const mockSelection = createFakePartial<vscode.Selection>({
      active: new vscode.Position(0, 0),
      start: new vscode.Position(0, 0),
      end: new vscode.Position(0, 0),
    });

    beforeEach(async () => {
      mockThread = createFakePartial<vscode.CommentThread>({
        label: 'Original label',
        range: new vscode.Range(0, 0, 0, 0),
      });
      const mockEditor = createFakePartial<vscode.TextEditor>({
        selection: mockSelection,
        document: {
          lineAt: jest.fn(),
          uri: createFakePartial<vscode.Uri>({}),
        },
        setDecorations: jest.fn(),
      });
      (vscode.window.activeTextEditor as unknown) = mockEditor;
      (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);
      await showChatWithEditor();
    });

    it('should update the thread label on selection change', async () => {
      const mockLabel = 'New selection for quick chat';
      jest.mocked(generateThreadLabel).mockReturnValue(mockLabel);

      await triggerTextEditorSelectionChange(
        createFakePartial<vscode.TextEditorSelectionChangeEvent>({
          textEditor: {
            document: {
              lineAt: jest.fn(),
              uri: {
                scheme: 'file',
                authority: 'other',
              },
            },
            selection: mockSelection,
          },
        }),
      );

      expect(generateThreadLabel).toHaveBeenCalled();
      expect(mockThread.label).toBe(mockLabel);
    });

    it('should not update the label when the thread comment is in focus', () => {
      jest.mocked(generateThreadLabel).mockClear();
      triggerTextEditorSelectionChange(
        createFakePartial<vscode.TextEditorSelectionChangeEvent>({
          textEditor: {
            document: {
              lineAt: jest.fn(),
              uri: {
                scheme: 'comment',
                authority: COMMENT_CONTROLLER_ID,
              },
            },
            selection: mockSelection,
          },
        }),
      );
      expect(generateThreadLabel).not.toHaveBeenCalled();
    });
  });

  describe('keybinding hints', () => {
    let mockEditor: vscode.TextEditor;
    let mockDecorationType: vscode.TextEditorDecorationType;
    const triggerConfigurationChange = createConfigurationChangeTrigger();

    beforeEach(() => {
      mockEditor = createFakePartial<vscode.TextEditor>({
        selection: createFakePartial<vscode.Selection>({ active: new vscode.Position(0, 0) }),
        document: createFakePartial<vscode.TextDocument>({
          lineAt: jest.fn().mockReturnValue({ isEmptyOrWhitespace: true }),
          uri: vscode.Uri.file('/test/file.ts'), // Default to a file URI
        }),
        setDecorations: jest.fn(),
      });

      mockDecorationType = createFakePartial<vscode.TextEditorDecorationType>({});
      jest.mocked(utils.createKeybindingHintDecoration).mockReturnValue(mockDecorationType);
    });

    const triggerSelectionChange = () =>
      triggerTextEditorSelectionChange({
        textEditor: mockEditor,
      } as vscode.TextEditorSelectionChangeEvent);

    it('shows hint when enabled and on empty line with correct range', async () => {
      const expectedPosition = new vscode.Position(2, 5);
      mockEditor.selection.active = expectedPosition;

      await triggerSelectionChange();

      expect(utils.createKeybindingHintDecoration).toHaveBeenCalled();
      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, [
        mockEditor.selection,
      ]);
    });

    it('does not show hint when disabled', async () => {
      mockConfig.keybindingHints.enabled = false;
      await triggerConfigurationChange();
      await triggerSelectionChange();

      expect(utils.createKeybindingHintDecoration).not.toHaveBeenCalled();
      expect(mockEditor.setDecorations).not.toHaveBeenCalled();
    });

    it('does not show hint on non-empty lines', async () => {
      mockEditor.document.lineAt = jest.fn().mockReturnValue({ isEmptyOrWhitespace: false });
      await triggerSelectionChange();

      expect(utils.createKeybindingHintDecoration).not.toHaveBeenCalled();
      expect(mockEditor.setDecorations).not.toHaveBeenCalled();
    });

    it('does not show hint for non-file documents', async () => {
      mockEditor = createFakePartial<vscode.TextEditor>({
        selection: createFakePartial<vscode.Selection>({ active: new vscode.Position(0, 0) }),
        document: createFakePartial<vscode.TextDocument>({
          lineAt: jest.fn().mockReturnValue({ isEmptyOrWhitespace: true }),
          uri: vscode.Uri.parse('output:test'), // Create a non-file URI
        }),
        setDecorations: jest.fn(),
      });

      await triggerSelectionChange();

      expect(utils.createKeybindingHintDecoration).not.toHaveBeenCalled();
      expect(mockEditor.setDecorations).not.toHaveBeenCalled();
    });
  });

  describe('gutter icon', () => {
    let mockEditor: vscode.TextEditor;
    let mockDecorationType: vscode.TextEditorDecorationType;
    let mockUri: vscode.Uri;

    beforeEach(async () => {
      mockUri = vscode.Uri.file('/test.ts');
      mockDecorationType = createFakePartial<vscode.TextEditorDecorationType>({
        dispose: jest.fn(),
      });
      jest.mocked(utils.createGutterIconDecoration).mockReturnValue(mockDecorationType);

      mockEditor = createFakePartial<vscode.TextEditor>({
        document: { uri: mockUri },
        selection: new vscode.Selection(0, 0, 0, 0),
        setDecorations: jest.fn(),
      });
      (vscode.window.activeTextEditor as unknown) = mockEditor;

      mockThread = createFakePartial<vscode.CommentThread>({
        uri: mockUri,
        range: new vscode.Range(0, 0, 1, 0),
        collapsibleState: vscode.CommentThreadCollapsibleState.Expanded,
      });
      (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);
    });

    it('shows gutter icon when chat is shown', async () => {
      await quickChat.show();

      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, [
        { range: new vscode.Range(mockThread.range.end, mockThread.range.end) },
      ]);
    });

    it('removes gutter icon when thread is collapsed', async () => {
      await quickChat.show();
      mockThread.collapsibleState = vscode.CommentThreadCollapsibleState.Collapsed;

      jest
        .mocked(vscode.window.onDidChangeTextEditorVisibleRanges)
        .mock.calls.forEach(([listener]) =>
          listener(
            createFakePartial<vscode.TextEditorVisibleRangesChangeEvent>({
              textEditor: mockEditor,
            }),
          ),
        );

      expect(mockDecorationType.dispose).toHaveBeenCalled();
    });

    it('shows gutter icon when in comment input', async () => {
      await quickChat.show();

      // Create new mock editor with comment input URI
      const commentInputEditor = createFakePartial<vscode.TextEditor>({
        document: {
          uri: createFakePartial<vscode.Uri>({
            authority: COMMENT_CONTROLLER_ID,
            scheme: 'comment',
          }),
        },
        selection: new vscode.Selection(0, 0, 0, 0),
        setDecorations: jest.fn(),
      });

      (vscode.window.visibleTextEditors as unknown) = [mockEditor];
      (vscode.window.activeTextEditor as unknown) = commentInputEditor;

      jest
        .mocked(vscode.window.onDidChangeTextEditorVisibleRanges)
        .mock.calls.forEach(([listener]) =>
          listener(
            createFakePartial<vscode.TextEditorVisibleRangesChangeEvent>({
              textEditor: commentInputEditor,
            }),
          ),
        );

      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, [
        { range: new vscode.Range(mockThread.range.end, mockThread.range.end) },
      ]);
    });

    it('does not show gutter icon for different documents', async () => {
      await quickChat.show();

      const differentEditor = createFakePartial<vscode.TextEditor>({
        document: { uri: vscode.Uri.file('/different.ts') },
        selection: new vscode.Selection(0, 0, 0, 0),
        setDecorations: jest.fn(),
      });
      (vscode.window.activeTextEditor as unknown) = differentEditor;

      jest
        .mocked(vscode.window.onDidChangeTextEditorVisibleRanges)
        .mock.calls.forEach(([listener]) =>
          listener(
            createFakePartial<vscode.TextEditorVisibleRangesChangeEvent>({
              textEditor: differentEditor,
            }),
          ),
        );

      expect(mockDecorationType.dispose).toHaveBeenCalled();
      expect(differentEditor.setDecorations).not.toHaveBeenCalledWith(
        mockDecorationType,
        expect.any(Array),
      );
    });
  });

  describe('document changes', () => {
    beforeEach(async () => {
      mockThread = createFakePartial<vscode.CommentThread>({
        uri: vscode.Uri.file('/test.ts'),
        range: new vscode.Range(0, 0, 5, 10),
      });

      await showChatWithEditor();
    });

    it('updates thread position when text changes', () => {
      const change = {
        range: new vscode.Range(2, 0, 2, 0),
        text: 'new line\n',
      };

      const newRange = new vscode.Range(0, 0, 6, 10);
      jest.mocked(utils.calculateThreadRange).mockReturnValue(newRange);

      expect(mockThread.range.end.line).toBe(5);

      jest.mocked(vscode.workspace.onDidChangeTextDocument).mock.calls.forEach(([listener]) =>
        listener(
          createFakePartial<vscode.TextDocumentChangeEvent>({
            document: createFakePartial<vscode.TextDocument>({ uri: mockThread.uri }),
            contentChanges: [change],
          }),
        ),
      );

      expect(mockThread.range.end.line).toBe(6);
    });
  });
});

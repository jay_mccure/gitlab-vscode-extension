import vscode from 'vscode';
import { v4 as uuidv4 } from 'uuid';
import { log } from '../log';
import { AiCompletionResponseMessageType } from '../api/graphql/ai_completion_response_channel';
import { CONFIG_NAMESPACE } from '../constants';
import { GitLabPlatformManagerForChat } from '../chat/get_platform_manager_for_chat';
import { GitLabChatApi, AiActionResponseType } from '../chat/gitlab_chat_api';
import { getActiveFileContext } from '../chat/gitlab_chat_file_context';
import { AIContextManager } from '../chat/ai_context_manager';
import { SPECIAL_MESSAGES } from '../chat/constants';
import {
  createCommentController,
  createComment,
  createLoaderComment,
  createChatResetComment,
  openAndShowDocument,
  setLoadingContext,
  getLastCommentContextValue,
  generateThreadLabel,
  provideCompletionItems,
  addCopyButtonsToCodeBlocks,
  MarkdownProcessorPipeline,
  COMMENT_CONTROLLER_ID,
  createKeybindingHintDecoration,
  createGutterIconDecoration,
  calculateThreadRange,
} from './utils';
import { COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT } from './constants';

export class QuickChat {
  #api: GitLabChatApi;

  #commentController: vscode.CommentController;

  #extensionContext: vscode.ExtensionContext;

  #response: vscode.MarkdownString | null = null;

  #thread: vscode.CommentThread | null = null;

  #nextExpectedChunkId: number = 1;

  #chunkBuffer: { [id: number]: string | undefined } = {};

  #markdownPipeline = new MarkdownProcessorPipeline([addCopyButtonsToCodeBlocks]);

  #disposables: vscode.Disposable[] = [];

  #keybindingHintDecoration: vscode.TextEditorDecorationType | null = null;

  #isKeybindingHintEnabled: vscode.WorkspaceConfiguration =
    vscode.workspace.getConfiguration(CONFIG_NAMESPACE)?.keybindingHints?.enabled;

  #gutterIconDecoration: vscode.TextEditorDecorationType | null = null;

  constructor(
    manager: GitLabPlatformManagerForChat,
    context: vscode.ExtensionContext,
    aiContextManager: AIContextManager,
  ) {
    this.#api = new GitLabChatApi(manager, [], aiContextManager);
    this.#commentController = createCommentController();
    this.#extensionContext = context;

    this.#disposables.push(
      this.#commentController,

      // Registers a completion item provider for quick actions (e.g. /tests)
      vscode.languages.registerCompletionItemProvider(
        { scheme: 'comment', pattern: '**' },
        { provideCompletionItems },
        '/',
      ),

      vscode.window.onDidChangeTextEditorSelection(event => {
        this.#updateThreadLabel(event);
        this.#updateKeybindingHint(event.textEditor);
      }),

      vscode.workspace.onDidChangeConfiguration(() => {
        this.#isKeybindingHintEnabled =
          vscode.workspace.getConfiguration(CONFIG_NAMESPACE)?.keybindingHints?.enabled;
      }),

      vscode.window.onDidChangeTextEditorVisibleRanges(() => this.#toggleGutterIcon()),

      vscode.workspace.onDidChangeTextDocument(event => {
        if (!this.#thread || event.document.uri !== this.#thread.uri) return;

        const updatedRange = calculateThreadRange(this.#thread, event.contentChanges[0]);

        if (!updatedRange) return;

        this.#thread.range = updatedRange;
        this.#toggleGutterIcon();
      }),
    );
  }

  async show() {
    const editor = vscode.window.activeTextEditor;
    if (!editor) return;

    const prompt = 'Ask a question or give an instruction...';
    const title = 'GitLab Duo Quick Chat';
    const userInput = await vscode.window.showInputBox({ placeHolder: prompt, title });
    if (!userInput?.trim()) return; // Exit if user cancels or input is empty

    this.#thread?.dispose();
    this.#api.clearChat().catch(log.error);

    const range = new vscode.Range(editor.selection.start, editor.selection.end);
    this.#commentController.options = { prompt };
    this.#thread = this.#commentController.createCommentThread(editor.document.uri, range, []);
    await setLoadingContext(false);
    this.#thread.collapsibleState = vscode.CommentThreadCollapsibleState.Expanded;
    this.#thread.label = generateThreadLabel(range);
    this.#toggleGutterIcon();

    await this.send({ text: userInput, thread: this.#thread });
  }

  async send(reply: vscode.CommentReply) {
    const isHandled = await this.#tryHandleSpecialMessage(reply.text);
    if (isHandled) return;

    this.#nextExpectedChunkId = 1;
    this.#chunkBuffer = {};
    this.#thread = reply.thread;
    this.#thread.comments = [
      ...(this.#thread.comments || []),
      createComment(reply.text, 'user', 'You'),
      createLoaderComment(), // shows 'GitLab Duo Chat is finding an answer' loading message
    ];

    const subscriptionId = uuidv4();
    await setLoadingContext(true); // disables the 'Send' button
    await this.#api.subscribeToUpdates(this.#subscriptionUpdateHandler.bind(this), subscriptionId);
    this.#submitQuestion(reply, subscriptionId).catch(log.error);
  }

  async #submitQuestion(reply: vscode.CommentReply, subId: string): Promise<AiActionResponseType> {
    await openAndShowDocument(reply.thread.uri);
    return this.#api.processNewUserPrompt(reply.text, subId, getActiveFileContext());
  }

  async #subscriptionUpdateHandler(data: AiCompletionResponseMessageType) {
    this.#hideLoaderComment();
    this.#createResponseComment();
    this.#appendResponseComment(data);
    await setLoadingContext(false);
  }

  async #tryHandleSpecialMessage(text: string) {
    switch (text.trim().toLowerCase()) {
      case SPECIAL_MESSAGES.CLEAR:
        await this.#clearChat();
        return true;
      case SPECIAL_MESSAGES.RESET:
        await this.#resetChat();
        return true;
      default:
        // Not a special message
        return false;
    }
  }

  async #clearChat() {
    await setLoadingContext(false);
    if (this.#thread) {
      this.#thread.comments = [];
    }
    this.#api.clearChat().catch(log.error);
    // FIXME: ensure input is focused once we have added this capability: https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1595
  }

  async #resetChat() {
    await setLoadingContext(false);
    if (this.#thread?.comments) {
      this.#thread.comments = [...this.#thread.comments, createChatResetComment()];
    }
    return this.#api.resetChat();
    // FIXME: ensure input is focused once we have added this capability: https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1595
  }

  #hideLoaderComment() {
    if (!this.#thread || getLastCommentContextValue(this.#thread) !== 'loader') return;
    this.#thread.comments = this.#thread.comments.slice(0, -1);
  }

  #createResponseComment() {
    if (!this.#thread || getLastCommentContextValue(this.#thread) === 'response') return;
    this.#response = new vscode.MarkdownString('');
    this.#thread.comments = [
      ...this.#thread.comments,
      createComment(this.#response, 'response', 'Duo'),
    ];
  }

  #appendResponseComment(data: AiCompletionResponseMessageType) {
    if (!this.#thread || !this.#response) return;

    if (data.chunkId) {
      this.#processChunk(data.chunkId, data.content);
    } else {
      this.#processFullResponse(data.content);
    }

    this.#thread.comments = [...this.#thread.comments];
  }

  #processChunk(chunkId: number, content: string) {
    // Chunks may arrive out of order, so we buffer them and process in sequence
    this.#chunkBuffer[chunkId] = content;
    const chunkContent = this.#chunkBuffer[this.#nextExpectedChunkId];
    if (!this.#response || !chunkContent) return;

    this.#response.appendMarkdown(chunkContent);
    this.#chunkBuffer[this.#nextExpectedChunkId] = undefined;
    this.#nextExpectedChunkId++;
  }

  #processFullResponse(content: string) {
    if (!this.#response) return;

    this.#response.isTrusted = { enabledCommands: [COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT] };
    this.#response.value = this.#markdownPipeline.process(content);
  }

  #updateThreadLabel(event: vscode.TextEditorSelectionChangeEvent) {
    if (!this.#thread) return;

    const { scheme, authority } = event.textEditor.document.uri;

    const isInCommentInput = scheme === 'comment' && authority === COMMENT_CONTROLLER_ID;

    //  we do not want to recalculate the label because when the focus moves to the input
    // the editor selection becomes empty and it is reflected on the label
    if (isInCommentInput) return;

    const editor = event.textEditor;

    if (editor) {
      const { selection } = editor;

      const range = new vscode.Range(selection.start, selection.end);

      const newText = generateThreadLabel(range);

      this.#thread.label = newText;
    }
  }

  #updateKeybindingHint(editor: vscode.TextEditor) {
    this.#keybindingHintDecoration?.dispose();

    const isKeybindingHintEnabled = this.#isKeybindingHintEnabled;
    const position = editor.selection.active;
    const currentLine = editor.document.lineAt(position.line);
    const isFile = editor.document.uri.scheme === 'file';

    if (!isKeybindingHintEnabled || !currentLine?.isEmptyOrWhitespace || !isFile) return;

    this.#keybindingHintDecoration = createKeybindingHintDecoration();
    editor.setDecorations(this.#keybindingHintDecoration, [editor.selection]);
  }

  #toggleGutterIcon() {
    this.#gutterIconDecoration?.dispose();
    const editor = vscode.window.activeTextEditor;
    const threadUri = this.#thread?.uri;
    const isInCommentInput = editor?.document.uri.authority === COMMENT_CONTROLLER_ID;
    const isCollapsed =
      this.#thread?.collapsibleState === vscode.CommentThreadCollapsibleState.Collapsed;

    // If we're in comment input, find the editor for the thread's document
    const targetEditor = isInCommentInput
      ? vscode.window.visibleTextEditors.find(e => e.document.uri === threadUri)
      : editor;

    const shouldShowIcon = targetEditor && !isCollapsed && targetEditor.document.uri === threadUri;

    if (!shouldShowIcon || !this.#thread) return;

    this.#gutterIconDecoration = createGutterIconDecoration(this.#extensionContext.extensionUri);
    targetEditor.setDecorations(this.#gutterIconDecoration, [
      {
        range: new vscode.Range(this.#thread.range.end, this.#thread.range.end),
      },
    ]);
  }

  dispose() {
    this.#disposables.forEach(disposable => disposable.dispose());
  }
}

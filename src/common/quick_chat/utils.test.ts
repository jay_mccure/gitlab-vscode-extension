import vscode from 'vscode';
import { createFakePartial } from '../test_utils/create_fake_partial';
import * as constants from '../constants';
import {
  createCommentController,
  createComment,
  createLoaderComment,
  openAndShowDocument,
  setLoadingContext,
  getLastCommentContextValue,
  generateThreadLabel,
  provideCompletionItems,
  addCopyButtonsToCodeBlocks,
  MarkdownProcessorPipeline,
  createChatResetComment,
  createKeybindingHintDecoration,
  createGutterIconDecoration,
  calculateThreadRange,
} from './utils';
import { COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT } from './constants';

describe('Quick Chat Utils', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('createCommentController', () => {
    it('creates a comment controller with correct parameters', () => {
      const mockCommentController = createFakePartial<vscode.CommentController>({});
      (vscode.comments.createCommentController as jest.Mock).mockReturnValue(mockCommentController);

      const result = createCommentController();

      expect(vscode.comments.createCommentController).toHaveBeenCalledWith(
        'duo-quick-chat',
        'Duo Quick Chat',
      );
      expect(result).toBe(mockCommentController);
    });
  });

  describe('createComment', () => {
    it('creates a comment with default values', () => {
      const body = 'Test comment';
      const comment = createComment(body);
      expect(comment).toEqual({
        body,
        mode: vscode.CommentMode.Preview,
        author: { name: '' },
        contextValue: '',
      });
    });

    it('creates a comment with custom values', () => {
      const body = new vscode.MarkdownString('Test comment');
      const contextValue = 'custom';
      const authorName = 'John Doe';
      const comment = createComment(body, contextValue, authorName);
      expect(comment).toEqual({
        body,
        mode: vscode.CommentMode.Preview,
        author: { name: authorName },
        contextValue,
      });
    });
  });

  describe('createLoaderComment', () => {
    it('creates a loader comment with correct properties', () => {
      const loaderComment = createLoaderComment();
      expect(loaderComment.body).toMatchObject({
        supportHtml: true,
        value: '<b>GitLab Duo Chat</b> is finding an answer',
      });
      expect(loaderComment.contextValue).toBe('loader');
    });
  });

  describe('createChatResetComment', () => {
    it('creates a chat reset comment with correct properties', () => {
      const resetComment = createChatResetComment();
      expect(resetComment.body).toMatchObject({
        supportHtml: true,
        value: '<hr /><em>New chat</em>',
      });
      expect(resetComment.contextValue).toBe('reset');
    });
  });

  describe('openAndShowDocument', () => {
    it('opens and show sthe document', async () => {
      const uri = vscode.Uri.file('test/path/to/file.txt');
      const mockEditor = createFakePartial<vscode.TextEditor>({ selection: undefined });

      (vscode.workspace.openTextDocument as jest.Mock).mockResolvedValue('mockDocument');
      (vscode.window.showTextDocument as jest.Mock).mockResolvedValue(mockEditor);

      await openAndShowDocument(uri);

      expect(vscode.workspace.openTextDocument).toHaveBeenCalledWith(uri);
      expect(vscode.window.showTextDocument).toHaveBeenCalledWith('mockDocument');
    });
  });

  describe('setLoadingContext', () => {
    it('executes setContext command with correct parameters', async () => {
      await setLoadingContext(true);
      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        'setContext',
        'duoCommentLoading',
        true,
      );
    });
  });

  describe('getLastCommentContextValue', () => {
    it('returns undefined for null thread', () => {
      expect(getLastCommentContextValue(null)).toBeUndefined();
    });

    it('returns context value of last comment in thread', () => {
      const mockThread = createFakePartial<vscode.CommentThread>({
        comments: [
          createComment('first', 'first'),
          createComment('second', 'second'),
          createComment('last', 'last'),
        ],
        uri: vscode.Uri.file('test/uri'),
        range: new vscode.Range(0, 0, 1, 1),
        collapsibleState: vscode.CommentThreadCollapsibleState.Collapsed,
        canReply: true,
        dispose: jest.fn(),
      });

      expect(getLastCommentContextValue(mockThread)).toBe('last');
    });

    it('returns undefined for empty thread', () => {
      const mockThread = createFakePartial<vscode.CommentThread>({
        comments: [],
        uri: vscode.Uri.file('test/uri'),
        range: new vscode.Range(0, 0, 1, 1),
        collapsibleState: vscode.CommentThreadCollapsibleState.Collapsed,
        canReply: true,
        dispose: jest.fn(),
      });

      expect(getLastCommentContextValue(mockThread)).toBeUndefined();
    });
  });

  describe('generatePlaceholderPrompt', () => {
    it('returns correct prompt for empty range', () => {
      const emptyRange = new vscode.Range(0, 0, 0, 0);
      emptyRange.isEmpty = true;

      expect(generateThreadLabel(emptyRange)).toBe(
        'Duo Quick Chat (select some code to add context)',
      );
    });

    it('returns correct prompt for single line range', () => {
      const singleLineRange = new vscode.Range(5, 0, 5, 10);
      expect(generateThreadLabel(singleLineRange)).toBe('Duo Quick Chat (include line 6)');
    });

    it('returns correct prompt for multi-line range', () => {
      const multiLineRange = new vscode.Range(10, 0, 15, 5);
      expect(generateThreadLabel(multiLineRange)).toBe('Duo Quick Chat (include lines 11-16)');
    });
  });

  describe('provideCompletionItems', () => {
    const mockPosition = createFakePartial<vscode.Position>({ character: 1 });
    const mockDocument = (text: string) =>
      createFakePartial<vscode.TextDocument>({
        lineAt: jest.fn().mockReturnValue({ text }),
      });

    it('should return undefined if line does not start with "/"', () => {
      const result = provideCompletionItems(mockDocument('some text'), mockPosition);

      expect(result).toBeUndefined();
    });

    it('should return completion items if line starts with "/"', () => {
      const result = provideCompletionItems(mockDocument('/'), mockPosition);
      const kind = vscode.CompletionItemKind.Text;

      const expectedActions = [
        {
          label: '/tests',
          insertText: 'tests',
          detail: 'Write tests for the selected snippet.',
          kind,
        },
        {
          label: '/refactor',
          insertText: 'refactor',
          detail: 'Refactor the selected snippet.',
          kind,
        },
        {
          label: '/explain',
          insertText: 'explain',
          detail: 'Explain the selected snippet.',
          kind,
        },
        {
          label: '/fix',
          insertText: 'fix',
          detail: 'Fix the selected code snippet.',
          kind,
        },
        {
          label: '/clear',
          insertText: 'clear',
          detail: 'Delete all messages in this conversation.',
          kind,
        },
        {
          label: '/reset',
          insertText: 'reset',
          detail: 'Reset conversation and ignore the previous messages.',
          kind,
        },
      ];

      expect(result).toEqual(expectedActions);
    });
  });

  describe('addCopyButtonsToCodeBlocks', () => {
    it('adds copy buttons to code blocks', () => {
      const markdown = '```javascript\nconst x = 1;\n```';
      const result = addCopyButtonsToCodeBlocks(markdown);
      expect(result).toContain('```javascript\nconst x = 1;\n```');
      expect(result).toContain(
        `[**Copy Snippet**](command:${COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT}?`,
      );
    });

    it('handles multiple code blocks', () => {
      const markdown = '```python\nprint("Hello")\n```\nSome text\n```ruby\nputs "World"\n```';
      const result = addCopyButtonsToCodeBlocks(markdown);
      expect(result).toContain('```python\nprint("Hello")\n```');
      expect(result).toContain('```ruby\nputs "World"\n```');
      expect(result.match(/\[\*\*Copy Snippet\*\*\]/g)).toHaveLength(2);
    });

    it('preserves non-code block content', () => {
      const markdown = 'Some text\n```css\nbody { color: red; }\n```\nMore text';
      const result = addCopyButtonsToCodeBlocks(markdown);
      expect(result).toContain('Some text');
      expect(result).toContain('```css\nbody { color: red; }\n```');
      expect(result).toContain('More text');
      expect(result).toContain(
        `[**Copy Snippet**](command:${COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT}?`,
      );
    });

    it('handles code blocks without language specification', () => {
      const markdown = '```\nconst y = 2;\n```';
      const result = addCopyButtonsToCodeBlocks(markdown);
      expect(result).toContain('```\nconst y = 2;\n```');
      expect(result).toContain(
        `[**Copy Snippet**](command:${COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT}?`,
      );
    });

    it('encodes command arguments correctly', () => {
      const markdown = '```json\n{"key": "value"}\n```';
      const result = addCopyButtonsToCodeBlocks(markdown);
      const encodedArgs = encodeURIComponent(JSON.stringify({ code: '{"key": "value"}' }));
      expect(result).toContain(
        `command:${COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT}?${encodedArgs}`,
      );
    });
  });

  describe('MarkdownProcessorPipeline', () => {
    let pipeline: MarkdownProcessorPipeline;

    beforeEach(() => {
      pipeline = new MarkdownProcessorPipeline([]);
    });

    it('should process markdown through all processors', () => {
      const processor1 = jest.fn((md: string) => `${md} processed 1`);
      const processor2 = jest.fn((md: string) => `${md} processed 2`);
      pipeline.addProcessor(processor1);
      pipeline.addProcessor(processor2);

      const result = pipeline.process('test');

      expect(processor1).toHaveBeenCalledWith('test');
      expect(processor2).toHaveBeenCalledWith('test processed 1');
      expect(result).toBe('test processed 1 processed 2');
    });

    it('should return the original markdown if no processors are added', () => {
      const result = pipeline.process('test');
      expect(result).toBe('test');
    });

    it('should allow adding processors after initialization', () => {
      const processor = jest.fn((md: string) => `${md} processed 3`);
      pipeline.addProcessor(processor);

      const result = pipeline.process('test');

      expect(processor).toHaveBeenCalledWith('test');
      expect(result).toBe('test processed 3');
    });
  });

  describe('createKeybindingHintDecoration', () => {
    let mockCreateTextEditorDecorationType: jest.Mock;

    beforeEach(() => {
      jest.resetAllMocks();
      mockCreateTextEditorDecorationType = jest.fn().mockReturnValue({});
      (vscode.window.createTextEditorDecorationType as jest.Mock).mockImplementation(
        mockCreateTextEditorDecorationType,
      );
    });

    afterEach(() => jest.restoreAllMocks());

    it.each`
      isOSX    | expectedContentText         | description
      ${false} | ${'(Alt+C) Duo Quick Chat'} | ${'non-macOS'}
      ${true}  | ${'⌥C Duo Quick Chat'}      | ${'macOS'}
    `('should create correct decoration for $description', ({ isOSX, expectedContentText }) => {
      jest.mocked(constants).IS_OSX = isOSX;

      createKeybindingHintDecoration();

      expect(mockCreateTextEditorDecorationType).toHaveBeenCalledWith({
        before: {
          contentText: expectedContentText,
          margin: '0 0 0 2ch',
          color: new vscode.ThemeColor('editorHint.foreground'),
          fontStyle: 'normal',
          textDecoration: 'none; filter: opacity(0.34);',
        },
      });
    });
  });

  describe('createGutterIconDecoration', () => {
    let mockCreateTextEditorDecorationType: jest.Mock;
    let mockJoinPath: jest.Mock;

    beforeEach(() => {
      mockCreateTextEditorDecorationType = jest.fn().mockReturnValue({});
      (vscode.window.createTextEditorDecorationType as jest.Mock).mockImplementation(
        mockCreateTextEditorDecorationType,
      );

      mockJoinPath = jest.fn().mockReturnValue(createFakePartial<vscode.Uri>({}));
      (vscode.Uri.joinPath as jest.Mock) = mockJoinPath;
    });

    it('creates decoration type with correct gutter icon properties', () => {
      const mockExtensionUri = createFakePartial<vscode.Uri>({});

      createGutterIconDecoration(mockExtensionUri);

      expect(mockJoinPath).toHaveBeenCalledWith(
        mockExtensionUri,
        'assets/icons/gitlab-duo-quick-chat.svg',
      );
      expect(mockCreateTextEditorDecorationType).toHaveBeenCalledWith({
        gutterIconPath: expect.any(Object),
      });
    });
  });

  describe('calculateThreadRange', () => {
    const thread = createFakePartial<vscode.CommentThread>({
      range: new vscode.Range(0, 0, 5, 10),
    });

    const createChange = (startLine: number, text: string) =>
      createFakePartial<vscode.TextDocumentContentChangeEvent>({
        range: new vscode.Range(startLine, 0, startLine, 0),
        text,
      });

    it('returns null for invalid inputs or changes after thread', () => {
      expect(calculateThreadRange(null, createChange(0, ''))).toBeNull();
      expect(calculateThreadRange(thread, null)).toBeNull();
      expect(calculateThreadRange(thread, createChange(6, '\n'))).toBeNull();
    });

    it('adjusts thread position when adding or removing lines', () => {
      const addLine = calculateThreadRange(thread, createChange(2, 'new\n'));
      expect(addLine?.end.line).toBe(6);

      const removeLine = calculateThreadRange(thread, createChange(2, ''));
      expect(removeLine?.end.line).toBe(5);
    });
  });
});

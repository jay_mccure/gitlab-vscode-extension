import vscode from 'vscode';
import { IS_OSX } from '../constants';
import { COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT } from './constants';

const COMMENT_LOADING_CONTEXT = 'duoCommentLoading';
export const COMMENT_CONTROLLER_ID = 'duo-quick-chat';

export const createCommentController = () =>
  vscode.comments.createCommentController(COMMENT_CONTROLLER_ID, 'Duo Quick Chat');

export const createComment = (
  body: vscode.MarkdownString | string,
  contextValue = '',
  authorName = '',
): vscode.Comment => ({
  body,
  mode: vscode.CommentMode.Preview,
  author: { name: authorName },
  contextValue,
});

export const createLoaderComment = () => {
  const loadingText = new vscode.MarkdownString('<b>GitLab Duo Chat</b> is finding an answer');
  loadingText.supportHtml = true;
  return createComment(loadingText, 'loader');
};

export const createChatResetComment = () => {
  const newChatText = new vscode.MarkdownString('<hr /><em>New chat</em>');
  newChatText.supportHtml = true;
  return createComment(newChatText, 'reset');
};

export const openAndShowDocument = async (uri: vscode.Uri) =>
  vscode.window.showTextDocument(await vscode.workspace.openTextDocument(uri));

export const setLoadingContext = (isLoading: boolean) =>
  vscode.commands.executeCommand('setContext', COMMENT_LOADING_CONTEXT, isLoading);

export const getLastCommentContextValue = (thread: vscode.CommentThread | null) =>
  thread?.comments.at(-1)?.contextValue;

export const generateThreadLabel = (range: vscode.Range) => {
  const startLine = range.start.line + 1;
  const endLine = range.end.line + 1;
  const prefix = 'Duo Quick Chat';

  if (range.isEmpty) return `${prefix} (select some code to add context)`;
  if (startLine === endLine) return `${prefix} (include line ${startLine})`;
  return `${prefix} (include lines ${startLine}-${endLine})`;
};

export const provideCompletionItems = (
  document: vscode.TextDocument,
  position: vscode.Position,
) => {
  const linePrefix = document.lineAt(position).text.substring(0, position.character);
  if (linePrefix.trim() !== '/') return undefined;

  const actions = [
    ['tests', 'Write tests for the selected snippet.'],
    ['refactor', 'Refactor the selected snippet.'],
    ['explain', 'Explain the selected snippet.'],
    ['fix', 'Fix the selected code snippet.'],
    ['clear', 'Delete all messages in this conversation.'],
    ['reset', 'Reset conversation and ignore the previous messages.'],
  ];

  return actions.map(([label, detail]) => {
    const item = new vscode.CompletionItem(`/${label}`, vscode.CompletionItemKind.Text);
    item.detail = detail;
    item.insertText = label;
    return item;
  });
};

type MarkdownProcessor = (markdown: string) => string;

/**
 * class that wil hold a pipeline of markdown processors.
 * Each processor will be a function that takes a markdown string
 * and returns a modified markdown string.
 */
export class MarkdownProcessorPipeline {
  #processors: MarkdownProcessor[];

  constructor(processors: MarkdownProcessor[]) {
    this.#processors = processors ?? [];
  }

  addProcessor(processor: (markdown: string) => string): void {
    this.#processors.push(processor);
  }

  process(markdown: string): string {
    return this.#processors.reduce((result, processor) => processor(result), markdown);
  }
}

/**
 * Function to parse the markdown and add "Copy" buttons to code blocks.
 */
export const addCopyButtonsToCodeBlocks: MarkdownProcessor = markdown => {
  // Regular expression to match code blocks in markdown (` ```language \n code... \n``` `)
  const codeBlockRegex = /^\s*```(\w+)?\s*$\n([\s\S]*?)\n^\s*```\s*$/gm;

  // Replace each code block with an appended copy button link
  const updatedMarkdown = markdown.replace(
    codeBlockRegex,
    (originalCode: string, language: string | undefined, codeContent: string) => {
      if (!codeContent) {
        return originalCode;
      }

      const languageIdentifier = language ?? '';

      const commandArgs = {
        code: codeContent,
      };

      const copyLink = `[**Copy Snippet**](command:${COMMAND_COPY_CODE_SNIPPET_FROM_QUICK_CHAT}?${encodeURIComponent(JSON.stringify(commandArgs))} "Click to copy this code snippet!")`;

      // Reconstruct the original code block and prepend the copy link
      return `\n${copyLink}\n\`\`\`${languageIdentifier}\n${codeContent}\n\`\`\``;
    },
  );

  return updatedMarkdown;
};

export const createKeybindingHintDecoration = () =>
  vscode.window.createTextEditorDecorationType({
    before: {
      contentText: IS_OSX ? '⌥C Duo Quick Chat' : '(Alt+C) Duo Quick Chat',
      margin: '0 0 0 2ch', // Add left margin
      color: new vscode.ThemeColor('editorHint.foreground'),
      fontStyle: 'normal',
      textDecoration: 'none; filter: opacity(0.34);',
    },
  });

export const createGutterIconDecoration = (
  extensionUri: vscode.Uri,
): vscode.TextEditorDecorationType =>
  vscode.window.createTextEditorDecorationType({
    gutterIconPath: vscode.Uri.joinPath(extensionUri, 'assets/icons/gitlab-duo-quick-chat.svg'),
  });

/**
 * Calculates the new range for a comment thread when text changes in the document.
 * If text is added or removed before the thread's position, the thread's end position
 * is adjusted to maintain its relative position in the document.
 */
export const calculateThreadRange = (
  thread: vscode.CommentThread | null,
  change: vscode.TextDocumentContentChangeEvent | null,
): vscode.Range | null => {
  if (!thread || !change) return null;

  // Only care about changes before thread end
  if (change.range.start.line > thread.range.end.line) return null;

  const newLines = change.text.split('\n').length - 1;
  const oldLines = change.range.end.line - change.range.start.line;
  const delta = newLines - oldLines;
  const newEnd = new vscode.Position(thread.range.end.line + delta, thread.range.end.character);

  return new vscode.Range(thread.range.start, newEnd);
};

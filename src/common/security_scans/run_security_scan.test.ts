import vscode from 'vscode';
import { BaseLanguageClient, DidSaveTextDocumentNotification } from 'vscode-languageclient';
import * as featureFlags from '../feature_flags/feature_flag_service';
import { createFakePartial } from '../test_utils/create_fake_partial';
import {
  getSecurityScannerConfiguration,
  SecurityScannerConfiguration,
} from '../utils/extension_configuration';
import { runSecurityScan } from './run_security_scan';

jest.mock('../utils/extension_configuration');
jest.mock('../feature_flags/feature_flag_service');

describe('runSecurityScan command', () => {
  let showInformationMessage: jest.Mock;
  let client: BaseLanguageClient;
  let doRunSecurityScan: () => Promise<void>;
  beforeEach(async () => {
    client = createFakePartial<BaseLanguageClient>({
      sendNotification: jest.fn(),
    });
    doRunSecurityScan = runSecurityScan(client);

    showInformationMessage = jest.fn(() => Promise.resolve());
    (vscode.window.showInformationMessage as jest.Mock).mockImplementation(showInformationMessage);
  });
  afterEach(() => {
    jest.resetAllMocks();
  });

  const mockConfigEnabled = (v: boolean) => {
    jest
      .mocked(getSecurityScannerConfiguration)
      .mockReturnValue(createFakePartial<SecurityScannerConfiguration>({ enabled: v }));
  };

  describe('when disabled', () => {
    it('with feature flag does nothing', async () => {
      jest.mocked(featureFlags.isEnabled).mockReturnValue(false);
      mockConfigEnabled(true);
      await doRunSecurityScan();
      expect(showInformationMessage).not.toHaveBeenCalled();
      expect(client.sendNotification).not.toHaveBeenCalled();
    });

    it('with configuration does nothing', async () => {
      jest.mocked(featureFlags.isEnabled).mockReturnValue(true);
      mockConfigEnabled(false);
      await doRunSecurityScan();
      expect(showInformationMessage).not.toHaveBeenCalled();
      expect(client.sendNotification).not.toHaveBeenCalled();
    });
  });
  describe('when enabled', () => {
    beforeEach(async () => {
      jest.mocked(featureFlags.isEnabled).mockReturnValue(true);
      mockConfigEnabled(true);
    });

    it('when there is no active editor shows a message', async () => {
      vscode.window.activeTextEditor = undefined;
      await doRunSecurityScan();
      expect(showInformationMessage).toHaveBeenCalledWith('GitLab Workflow: No open file.');
    });

    describe('when there is an active editor', () => {
      beforeEach(async () => {
        vscode.window.activeTextEditor = createFakePartial<vscode.TextEditor>({
          document: createFakePartial<vscode.TextDocument>({
            uri: vscode.Uri.file('file.txt'),
            languageId: 'text',
            version: 1,
            getText: jest.fn().mockReturnValue('content'),
          }),
        });
      });

      it('sends a notification', async () => {
        await doRunSecurityScan();
        expect(client.sendNotification).toHaveBeenCalledWith(DidSaveTextDocumentNotification.type, {
          text: 'content',
          textDocument: {
            languageId: 'text',
            uri: 'file://file.txt',
            version: 1,
          },
        });
      });
    });
  });
});

import {
  BaseLanguageClient,
  DidSaveTextDocumentNotification,
  DidSaveTextDocumentParams,
} from 'vscode-languageclient';
import vscode from 'vscode';
import { isEnabled, FeatureFlag } from '../feature_flags/feature_flag_service';
import { getSecurityScannerConfiguration } from '../utils/extension_configuration';

export const COMMAND_RUN_SECURITY_SCAN = 'gl.runSecurityScan';

export const runSecurityScan = (client: BaseLanguageClient) => async () => {
  if (!(isEnabled(FeatureFlag.RemoteSecurityScans) && getSecurityScannerConfiguration().enabled)) {
    return;
  }

  const editor = vscode.window.activeTextEditor;

  if (!editor) {
    await vscode.window.showInformationMessage('GitLab Workflow: No open file.');
    return;
  }

  const content = editor.document.getText();
  const fileUri = editor.document.uri;
  await client.sendNotification(DidSaveTextDocumentNotification.type, {
    textDocument: {
      uri: fileUri.toString(),
      languageId: editor.document.languageId,
      version: editor.document.version,
    },
    text: content,
  } as DidSaveTextDocumentParams);
};

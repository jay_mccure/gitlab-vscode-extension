import * as vscode from 'vscode';
import { getWebviewContent } from './get_ls_webview_content';
import { applyMiddleware, WebviewContainerMiddleware } from './middleware';

type WebviewControllerProps = {
  viewId: string;
  url: URL;
  title: string;
  description?: string;
  middlewares?: WebviewContainerMiddleware[];
};

export class LsWebviewController implements vscode.WebviewViewProvider {
  #viewId: string;

  #url: URL;

  #title: string;

  #view: vscode.WebviewView | undefined;

  #middlewares: WebviewContainerMiddleware[];

  constructor({ viewId, url, title, middlewares }: WebviewControllerProps) {
    this.#viewId = viewId;
    this.#url = url;
    this.#title = title;
    this.#middlewares = middlewares ?? [];
  }

  async resolveWebviewView(webviewView: vscode.WebviewView): Promise<void> {
    this.#view = webviewView;
    this.#view.webview.options = {
      enableScripts: true,
    };

    this.#view.title = this.#title;

    this.#view.webview.html = getWebviewContent(this.#url, this.#view.title);

    if (this.#middlewares.length > 0) {
      applyMiddleware(this.#view, this.#middlewares);
    }
  }

  async show() {
    if (this.#view) {
      if (!this.#view.visible) {
        this.#view.show();
      }
    } else {
      await vscode.commands.executeCommand(`${this.#viewId}.focus`);
    }
  }
}

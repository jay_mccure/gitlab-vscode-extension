import * as vscode from 'vscode';
import { DUO_CHAT_WEBVIEW_ID, DUO_WORKFLOW_WEBVIEW_ID } from '../constants';
import { setupWebviews } from './setup_webviews';
import { WebviewInfoProvider } from './webview_info_provider';
import { WebviewThemePublisher } from './theme/types';
import { LsWebviewController } from './ls_webview_controller';
import { getWebviewContent } from './get_ls_webview_content';

jest.mock('./middleware');
jest.mock('./get_ls_webview_content');
jest.mock('./ls_webview_controller');
jest.mock('vscode', () => ({
  window: {
    createWebviewPanel: jest.fn(),
    registerWebviewViewProvider: jest.fn(),
  },
  commands: {
    registerCommand: jest.fn(),
  },
  ViewColumn: {
    One: 1,
  },
  Disposable: jest.fn(),
}));

describe('setupWebviews', () => {
  let mockProvider: WebviewInfoProvider;
  let mockThemePublisher: WebviewThemePublisher;
  const webview = {} as vscode.Webview;

  beforeEach(() => {
    jest.clearAllMocks();

    mockProvider = {
      getWebviewInfos: jest.fn().mockResolvedValue([]),
    };
    mockThemePublisher = {
      publishWebviewTheme: jest.fn(),
    };

    vscode.window.createWebviewPanel = jest.fn().mockReturnValue({ webview });
  });

  describe('for the panel webviews', () => {
    it('should setup panel webviews correctly', async () => {
      mockProvider.getWebviewInfos = jest.fn().mockResolvedValue([
        {
          id: DUO_CHAT_WEBVIEW_ID,
          title: 'Duo Chat',
          uris: ['https://example.com/duo-chat'],
        },
      ]);

      const disposable = await setupWebviews(mockProvider, mockThemePublisher);

      expect(vscode.window.registerWebviewViewProvider).toHaveBeenCalledTimes(1);
      expect(vscode.commands.registerCommand).toHaveBeenCalledTimes(1);
      expect(LsWebviewController).toHaveBeenCalledTimes(1);
      expect(disposable).toBeDefined();
    });

    it('should create show command for panel webviews', async () => {
      mockProvider.getWebviewInfos = jest.fn().mockResolvedValue([
        {
          id: DUO_CHAT_WEBVIEW_ID,
          title: 'Duo Chat',
          uris: ['https://example.com/duo-chat'],
        },
      ]);

      await setupWebviews(mockProvider, mockThemePublisher);

      expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
        'gl.webview.duo-chat.show',
        expect.any(Function),
      );
    });
  });

  describe('for the editor webviews', () => {
    it('should setup editor webviews correctly', async () => {
      mockProvider.getWebviewInfos = jest.fn().mockResolvedValue([
        {
          id: DUO_WORKFLOW_WEBVIEW_ID,
          title: 'Duo Workflow',
          uris: ['https://example.com/duo-workflow'],
        },
      ]);

      const disposable = await setupWebviews(mockProvider, mockThemePublisher);

      expect(vscode.commands.registerCommand).toHaveBeenCalledTimes(1);
      expect(disposable).toBeDefined();
    });

    it('should create show command for editor webviews', async () => {
      mockProvider.getWebviewInfos = jest.fn().mockResolvedValue([
        {
          id: DUO_WORKFLOW_WEBVIEW_ID,
          title: 'Duo Workflow',
          uris: ['https://example.com/duo-workflow'],
        },
      ]);

      await setupWebviews(mockProvider, mockThemePublisher);

      expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
        'gl.webview.duo-workflow.show',
        expect.any(Function),
      );
    });
  });

  it('should correctly setup both panel and editor webviews when there are both', async () => {
    mockProvider.getWebviewInfos = jest.fn().mockResolvedValue([
      {
        id: DUO_CHAT_WEBVIEW_ID,
        title: 'Duo Chat',
        uris: ['https://example.com/duo-chat'],
      },
      {
        id: DUO_WORKFLOW_WEBVIEW_ID,
        title: 'Duo Workflow',
        uris: ['https://example.com/duo-workflow'],
      },
    ]);

    const disposable = await setupWebviews(mockProvider, mockThemePublisher);

    expect(vscode.commands.registerCommand).toHaveBeenCalledTimes(2);
    expect(jest.mocked(vscode.commands.registerCommand).mock.calls).toEqual([
      [`gl.webview.${DUO_CHAT_WEBVIEW_ID}.show`, expect.any(Function)],
      [`gl.webview.${DUO_WORKFLOW_WEBVIEW_ID}.show`, expect.any(Function)],
    ]);
    expect(vscode.window.registerWebviewViewProvider).toHaveBeenCalledTimes(1);
    expect(vscode.window.registerWebviewViewProvider).toHaveBeenCalledWith(
      `gl.webview.${DUO_CHAT_WEBVIEW_ID}`,
      expect.anything(),
      {
        webviewOptions: { retainContextWhenHidden: true },
      },
    );

    expect(LsWebviewController).toHaveBeenCalledTimes(1);
    expect(LsWebviewController).toHaveBeenCalledWith(
      expect.objectContaining({ viewId: `gl.webview.${DUO_CHAT_WEBVIEW_ID}` }),
    );

    expect(disposable).toBeDefined();
  });

  it('should handle empty webview infos', async () => {
    mockProvider.getWebviewInfos = jest.fn().mockResolvedValue([]);

    const disposable = await setupWebviews(mockProvider, mockThemePublisher);

    expect(vscode.window.registerWebviewViewProvider).not.toHaveBeenCalled();
    expect(vscode.commands.registerCommand).not.toHaveBeenCalled();
    expect(LsWebviewController).not.toHaveBeenCalled();
    expect(disposable).toBeDefined();
  });

  it('should create webview panel when show command is executed for editor webviews', async () => {
    const htmlContent = '<h1>Foo Bar</h1>';
    jest.mocked(getWebviewContent).mockReturnValue(htmlContent);

    mockProvider.getWebviewInfos = jest.fn().mockResolvedValue([
      {
        id: DUO_WORKFLOW_WEBVIEW_ID,
        title: 'Duo Workflow',
        uris: ['https://example.com/duo-workflow'],
      },
    ]);

    await setupWebviews(mockProvider, mockThemePublisher);

    const showCommandHandler = (vscode.commands.registerCommand as jest.Mock).mock.calls[0][1];
    await showCommandHandler();

    expect(vscode.window.createWebviewPanel).toHaveBeenCalledWith(
      DUO_WORKFLOW_WEBVIEW_ID,
      'Duo Workflow',
      {
        viewColumn: vscode.ViewColumn.One,
        preserveFocus: true,
      },
    );
    expect(webview.options).toEqual({
      enableScripts: true,
    });
    expect(webview.html).toBe(htmlContent);
  });
});

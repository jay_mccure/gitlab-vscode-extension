import * as vscode from 'vscode';
import { DUO_CHAT_WEBVIEW_ID, DUO_WORKFLOW_WEBVIEW_ID } from '../constants';
import { LsWebviewController } from './ls_webview_controller';
import { WebviewInfo, WebviewInfoProvider } from './webview_info_provider';
import { WebviewThemePublisher } from './theme/types';
import { createThemeHandlerMiddleware } from './theme/create_theme_handler_middleware';
import { getWebviewContent } from './get_ls_webview_content';
import { applyMiddleware } from './middleware';

// webviews that show in the VS Code panels, sidebar or other custom views (like activity bar)
const PANEL_WEBVIEW_IDS = [DUO_CHAT_WEBVIEW_ID];

// webviews in the editor area (i.e. tabs)
const EDITOR_WEBVIEW_IDS = [DUO_WORKFLOW_WEBVIEW_ID];

const setupEditorWebview = async (
  webviewInfo: WebviewInfo,
  themePublisher: WebviewThemePublisher,
): Promise<vscode.Disposable> => {
  const viewId = `gl.webview.${webviewInfo.id}`;

  const middlewares = [createThemeHandlerMiddleware(themePublisher)];
  const disposables: vscode.Disposable[] = [];

  disposables.push(
    vscode.commands.registerCommand(`${viewId}.show`, async () => {
      const panel = vscode.window.createWebviewPanel(webviewInfo.id, webviewInfo.title, {
        viewColumn: vscode.ViewColumn.One,
        preserveFocus: true,
      });
      panel.webview.options = {
        enableScripts: true,
      };
      panel.webview.html = getWebviewContent(new URL(webviewInfo.uris[0]), webviewInfo.title); // FIXME this is not the right way to pick the uri, this should be platform dependent

      applyMiddleware(panel, middlewares);
    }),
  );

  return new vscode.Disposable(() => disposables.forEach(x => x.dispose()));
};

const setupPanelWebview = async (
  webviewInfo: WebviewInfo,
  themePublisher: WebviewThemePublisher,
): Promise<vscode.Disposable> => {
  const viewId = `gl.webview.${webviewInfo.id}`;

  const middlewares = [createThemeHandlerMiddleware(themePublisher)];

  const controller = new LsWebviewController({
    viewId,
    url: new URL(webviewInfo.uris[0]), // FIXME this is not the right way to pick the uri, this should be platform dependent
    title: webviewInfo.title,
    middlewares,
  });

  const disposables: vscode.Disposable[] = [];

  // FIXME: we should have a mechanism to restore webview state because VS Code prefers to destroy the web pages when they are not visible (the `retainContextWhenHidden` should be false)
  disposables.push(
    vscode.window.registerWebviewViewProvider(viewId, controller, {
      webviewOptions: { retainContextWhenHidden: true },
    }),
  );

  disposables.push(
    vscode.commands.registerCommand(`${viewId}.show`, async () => {
      await controller.show();
    }),
  );

  return new vscode.Disposable(() => disposables.forEach(x => x.dispose()));
};

export const setupWebviews = async (
  provider: WebviewInfoProvider,
  themePublisher: WebviewThemePublisher,
): Promise<vscode.Disposable> => {
  const allInfos = await provider.getWebviewInfos();
  const panelWebviewInfos = allInfos.filter(i => PANEL_WEBVIEW_IDS.includes(i.id));
  const editorWebviewInfos = allInfos.filter(i => EDITOR_WEBVIEW_IDS.includes(i.id));
  const disposables = await Promise.all([
    ...panelWebviewInfos.map(webviewInfo => setupPanelWebview(webviewInfo, themePublisher)),
    ...editorWebviewInfos.map(webviewInfo => setupEditorWebview(webviewInfo, themePublisher)),
  ]);

  return new vscode.Disposable(() => disposables.forEach(x => x.dispose()));
};

import { createOAuthAccount } from '../../test_utils/entities';
import { AccountService } from '../account_service';
import { createExtensionContext } from '../../../common/test_utils/entities';
import { GitLabAuthenticationProvider } from './gitlab_authentication_provider';

jest.mock('../../commands/openers');
jest.mock('../../gitlab/gitlab_service');
jest.useFakeTimers();

describe('GitLabAuthenticationProvider', () => {
  let accountService: AccountService;

  beforeEach(async () => {
    accountService = new AccountService();
    await accountService.init(createExtensionContext());
  });

  describe('getting existing session', () => {
    it('gets a session if there is existing oauth account', async () => {
      await accountService.addAccount(createOAuthAccount());
      const provider = new GitLabAuthenticationProvider(accountService);

      const sessions = await provider.getSessions(['api']);

      expect(sessions).toHaveLength(1);
      expect(sessions[0].accessToken).toBe(createOAuthAccount().token);
    });
  });
});

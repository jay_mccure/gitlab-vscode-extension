import vscode from 'vscode';
import { accountService, AccountService } from '../account_service';
import { OAuthAccount } from '../../../common/platform/gitlab_account';
import { sort } from '../../utils/sort';

const scopesString = (scopes: readonly string[]) => sort(scopes).join();

const convertAccountToAuthenticationSession = (
  account: OAuthAccount,
): vscode.AuthenticationSession => ({
  accessToken: account.token,
  id: account.id,
  scopes: account.scopes,
  account: {
    id: account.id,
    label: `${account.instanceUrl} (${account.username})`,
  },
});

export class GitLabAuthenticationProvider implements vscode.AuthenticationProvider {
  #eventEmitter =
    new vscode.EventEmitter<vscode.AuthenticationProviderAuthenticationSessionsChangeEvent>();

  #accountService: AccountService;

  constructor(as = accountService) {
    this.#accountService = as;
  }

  onDidChangeSessions = this.#eventEmitter.event;

  async getSessions(scopes?: readonly string[]): Promise<readonly vscode.AuthenticationSession[]> {
    return this.#accountService
      .getAllAccounts()
      .filter((a): a is OAuthAccount => a.type === 'oauth')
      .filter(a => !scopes || scopesString(a.scopes) === scopesString(scopes))
      .map(convertAccountToAuthenticationSession);
  }

  async createSession(/* scopes: readonly string[] */): Promise<vscode.AuthenticationSession> {
    // This would show to 3rd party extensions depending on our Auth provider.
    throw new Error(
      'Creating `gitlab.com` sessions is no longer supported. It will be implemented again and better in https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1381',
    );
  }

  async removeSession(sessionId: string): Promise<void> {
    await this.#accountService.removeAccount(sessionId);
  }
}

export const gitlabAuthenticationProvider = new GitLabAuthenticationProvider();

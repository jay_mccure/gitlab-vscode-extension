### Instructions for running End-to-End tests locally

1. Clone this repository
2. `cd gitlab-vscode-extension`
3. `npm ci`
4. `npm run package`
5. _ensure vsix file generated_
6. `cd test/e2e`
7. `npm install`
8. Create a valid GitLab [PAT](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with the `api` scope
9. `TEST_GITLAB_TOKEN=<PAT from previous step> npm run test:e2e` OR to run `E2E Tests` from the [Run view](https://code.visualstudio.com/Docs/editor/debugging#_run-view), add the PAT from the previous step to `TEST_GITLAB_TOKEN` in [launch.json](https://code.visualstudio.com/Docs/editor/debugging#_launch-configurations) before running.

Note: the first time `test:e2e` is run, VSCode will be downloaded.

After the tests finish, an Allure report will be generated in `test/e2e/allure-report`.
If run in a CI pipeline, the Allure report will be stored in job artifacts for 10 days.

import { browser } from '@wdio/globals';
import { waitForPromptTitleToContain } from './command_palette_helpers.js';
import { dismissAllNotifications, waitForNotification } from './notification_helpers.js';

/**
 * Executes the 'GitLab: Authenticate' command. Retries if extension is not loaded yet.
 *
 * @async
 * @returns {prompt}
 */
const invokeAuthPrompt = async () => {
  const workbench = await browser.getWorkbench();
  let prompt;

  await browser.waitUntil(
    async () => {
      prompt = await workbench.executeCommand('GitLab: Authenticate');

      const promptText = await prompt.getTitle();
      return typeof promptText === 'string' && promptText.includes('Select GitLab instance');
    },
    {
      timeout: 15000,
      timeoutMsg: `Command palette did not have 'GitLab: Authenticate' after 15 seconds".`,
    },
  );

  return prompt;
};

export const selectPatAuthenticationAndOpenTokenInput = async () => {
  await dismissAllNotifications();

  const prompt = await invokeAuthPrompt();
  await prompt.selectQuickPick('https://gitlab.com');

  await waitForPromptTitleToContain(prompt, 'Select authentication method');
  await prompt.selectQuickPick('Personal Access Token');

  await waitForPromptTitleToContain(prompt, 'Do you want to create a new token?');
  await prompt.selectQuickPick('Enter an existing token');

  await expect(prompt.input$).toHaveAttr('type', 'password', {
    message: 'Input field is not masked for passwords.',
  });

  return prompt;
};

/**
 * Completes authorization with environment variable `TEST_GITLAB_TOKEN` for the GitLab extension
 *
 * @async
 * @returns {Promise<void>}
 */
export const completeAuth = async () => {
  if (!process.env.TEST_GITLAB_TOKEN) {
    throw new Error('TEST_GITLAB_TOKEN environment variable is not set!');
  }

  const prompt = await selectPatAuthenticationAndOpenTokenInput();

  await prompt.setText(process.env.TEST_GITLAB_TOKEN);
  await prompt.confirm();

  await waitForNotification('Added GitLab account for user');
};

export const CHAT_MODEL_ROLES = {
  user: 'user',
  system: 'system',
  assistant: 'assistant',
};

export const MESSAGES_WITHOUT_RESPONSES = {
  CLEAN: '/clean',
  CLEAR: '/clear',
  RESET: '/reset',
};

export default {};

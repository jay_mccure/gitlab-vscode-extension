import Vue from 'vue';
import App from './App.vue';

Vue.config.productionTip = false;

const el = document.getElementById('app');
const initialState = JSON.parse(document.querySelector('[data-initial-state]').innerHTML);

new Vue({
  el,
  render(createElement) {
    return createElement(App, {
      props: {
        ...initialState,
      },
    });
  },
}).$mount();
